program ReceptorColector;

uses
  System.StartUpCopy,
  FMX.Forms,
  uReceptor in 'uReceptor.pas' {FmReceptorColector},
  Globales in 'support\Globales.pas',
  Abstractas in 'support\Abstractas.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TFmReceptorColector, FmReceptorColector);
  Application.Run;
end.
