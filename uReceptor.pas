unit uReceptor;

interface

uses
  System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Layouts,
  FMX.ListBox, FMX.StdCtrls, FMX.Controls.Presentation, IPPeerClient,
  IPPeerServer, System.Tether.Manager, System.Tether.AppProfile,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf,
  FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async,
  FireDAC.Phys, FireDAC.Phys.MSSQL, FireDAC.Phys.MSSQLDef, FireDAC.FMXUI.Wait,
  Data.DB, FireDAC.Comp.Client, FireDAC.Stan.Param, FireDAC.DatS,
  FireDAC.DApt.Intf, FireDAC.DApt, FireDAC.Comp.DataSet,
  FMX.Objects,Firedac.stan.StorageXML,System.IniFiles, FireDAC.Stan.StorageBin;

type
  TFmReceptorColector = class(TForm)
    CalloutPanel1: TCalloutPanel;
    Label1: TLabel;
    VertScrollBox1: TVertScrollBox;
    lbCodigos: TListBox;
    ReceptorColector: TTetheringManager;
    stringReciverProfile: TTetheringAppProfile;
    pnInformar: TPanel;
    btnLogin: TButton;
    ListBoxItem1: TListBoxItem;
    StyleBook1: TStyleBook;
    Conn: TFDConnection;
    qry: TFDQuery;
    Layout1: TLayout;
    cirCantConexiones: TCircle;
    lblCantConexiones: TLabel;
    fdDetalle: TFDMemTable;
    fdDetalleiCantidad: TFloatField;
    fdDetalleidTipoDoc: TStringField;
    fdDetalleidSucursal: TIntegerField;
    fdDetalleidProveedor: TIntegerField;
    fdDetalleidNumDoc: TIntegerField;
    fdDetalleidNumLinea: TIntegerField;
    fdDetalleidProducto: TIntegerField;
    fdDetallesCodProducto: TStringField;
    fdDetallesCodBarra: TStringField;
    fdDetallesNombre: TStringField;
    fdDetalleiCantidadRecibida: TFloatField;
    fdDetalleiCantidadRecepcionada: TFloatField;
    fdProveedores: TFDMemTable;
    fdProveedoresidProveedor: TIntegerField;
    fdProveedoressRazonSocial: TStringField;
    lblBD: TLabel;
    fdCabecera: TFDMemTable;
    fdCabecerasNumDoc: TStringField;
    fdCabecerafDocumento: TDateTimeField;
    fdCabeceraidTipoDoc: TStringField;
    fdCabeceraidProveedor: TIntegerField;
    fdCabeceraidSucursal: TIntegerField;
    fdCabeceraidNumDoc: TIntegerField;
    fdCabecerasRazonSocial: TStringField;
    fdCabecerafechaFloat: TFloatField;
    qryRecepcionFiltro1: TFDQuery;
    qryRecepcionFiltro2: TFDQuery;
    qryRecepcionFiltro3: TFDQuery;
    fdRecepcionDetalle: TFDMemTable;
    procedure ReceptorColectorRequestManagerPassword(const Sender: TObject;
      const ARemoteIdentifier: string; var Password: string);
    procedure stringReciverProfileResourceReceived(const Sender: TObject;
      const AResource: TRemoteResource);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnLoginClick(Sender: TObject);
    procedure buscarReceptorRemoto;
    procedure FormCreate(Sender: TObject);
    procedure stringReciverProfileDisconnect(const Sender: TObject;
      const AProfileInfo: TTetheringProfileInfo);
    procedure stringReciverProfileAfterConnectProfile(const Sender: TObject;
      const AProfileInfo: TTetheringProfileInfo);
    procedure CargarRecepcionMercaderia(idRemoto: string);
    procedure CargarRecepcionMercaderiaDetalle(idRemoto,idTipoDoc: string; idProveedor, idSucursal ,idNumDoc: Integer);
    procedure CargarProveedores(idRemoto: string);
    procedure CargarRecepcionFiltro1(idRemoto: string);
    procedure CargarRecepcionFiltro2(idRemoto: string; ID_F1: string);
    procedure CargarRecepcionFiltro3(idRemoto: string; ID_F1, ID_F2: string);
    Function LeerINI():Boolean;
    procedure ConnAfterConnect(Sender: TObject);
  private
    { Private declarations }
    _conexiones: integer;
    procedure Split(Delimiter: Char; Str: string; ListOfStrings: TStrings);
    procedure CargarListBox(res: TStringList);
    procedure ConsultarCodigo(idRemoto: string; codigo: string);
    procedure CargarEnBD(idRemoto: string; res: TStringList);
    procedure CargarRecepcionEnBD(idRemoto: string; res: TStringStream);
    procedure CargarRecepcionSinOCEnBD(idRemoto: string; res: TStringStream);
    procedure AgregarConexion(cant: integer);
    procedure CargarError(err: string);
  public
    { Public declarations }
  end;

var
  FmReceptorColector: TFmReceptorColector;

implementation

uses
  System.DateUtils, System.SysUtils, Globales;

{$R *.fmx}

procedure TFmReceptorColector.Split(Delimiter: Char; Str: string; ListOfStrings: TStrings) ;
begin
   ListOfStrings.Clear;
   ListOfStrings.Delimiter       := Delimiter;
   ListOfStrings.StrictDelimiter := True; // Requires D2006 or newer.
   ListOfStrings.DelimitedText   := Str;
end;


procedure TFmReceptorColector.FormClose(Sender: TObject;
  var Action: TCloseAction);
var
  I: Integer;
begin
  for I := 0 to ReceptorColector.PairedManagers.Count-1 do
   try
      ReceptorColector.UnPairManager(ReceptorColector.PairedManagers[I]);
   except
    on e:exception do
     CargarError(e.Message);
   end;
end;

procedure TFmReceptorColector.FormCreate(Sender: TObject);
begin
  FormatSettings.DecimalSeparator := '.';
  FormatSettings.ThousandSeparator := ' ';
  LeerINI();
  ReceptorColector.AutoConnect();
end;

procedure TFmReceptorColector.FormShow(Sender: TObject);
begin
//  ReceptorColector.AutoConnect();
  lbCodigos.Clear;
//  ReceptorColector.DiscoverManagers;
end;

Function TFmReceptorColector.LeerINI():Boolean;
var
  strCon,sIni: string;
  iFile: TIniFile;
begin
 try
    sIni:=ExtractFilePath(ParamStr(0))+'ReceptorColector.ini';
    if FileExists(sIni) then
      begin
       try
        iFile := TIniFile.Create(sIni);
        strCon:=iFile.ReadString('config','Conexion','');
        iFile.Free;
        Result:=True;
        //Conn.ConnectionString:=strCon;
        Conn.Connected:=True;
       except
        on e:exception do
         begin
           CargarError('Al intentar crear String de Coneccion se produjo el siguiente error.'+e.Message);
         end;
       end;
      end
    else
      begin
        CargarError('No se encuentra el archivo ReceptorColector.ini'#10#13+sIni);
        Result:=False;
      end
 except
  on e:exception do
   CargarError(e.Message);
 end;
end;

procedure TFmReceptorColector.ReceptorColectorRequestManagerPassword(
  const Sender: TObject; const ARemoteIdentifier: string; var Password: string);
begin
  Password := '1234';
end;

procedure TFmReceptorColector.stringReciverProfileAfterConnectProfile(
  const Sender: TObject; const AProfileInfo: TTetheringProfileInfo);
begin
  AgregarConexion(1);
end;

procedure TFmReceptorColector.stringReciverProfileDisconnect(
  const Sender: TObject; const AProfileInfo: TTetheringProfileInfo);
begin
  AgregarConexion(-1);
end;


procedure TFmReceptorColector.CargarRecepcionMercaderiaDetalle(idRemoto,idTipoDoc: string; idProveedor, idSucursal ,idNumDoc: Integer);
var
  precio, nombre: String;
  I: Integer;
  st:TStringList;
  res:string;
  //Campos
  oStr: TStringStream;
begin
  try
    res:='';
    oStr:=TStringStream.Create;
    qry.sql.text := 'EXEC getRecepcionMercaderiaDetalle '+IntToStr(idProveedor)+', '+
                                                          QuotedStr(idTipoDoc)+', '+
                                                          IntToStr(idSucursal)+', '+
                                                          IntToStr(idNumDoc);
    try
      qry.Open();
      qry.First;
      fdDetalle.Open;
      fdDetalle.EmptyDataSet;
      while not qry.eof do
       begin
         try
           fdDetalle.Append;
             fdDetalle.FieldByName('idTipoDoc').Value:=idTipoDoc;
             fdDetalle.FieldByName('idProveedor').Value:=idProveedor;
             fdDetalle.FieldByName('idSucursal').Value:=idSucursal;
             fdDetalle.FieldByName('idNumDoc').Value:=idNumDoc;
             fdDetalle.FieldByName('idNumLinea').Value:=qry.FieldByName('idNumLinea').AsInteger;
             fdDetalle.FieldByName('idProducto').Value:=qry.FieldByName('idProducto').AsInteger;
             fdDetalle.FieldByName('sCodProducto').Value:=qry.FieldByName('sCodProducto').AsString;
             fdDetalle.FieldByName('sCodBarra').Value:=qry.FieldByName('sCodBarra').AsString;
             fdDetalle.FieldByName('sNombre').Value:=qry.FieldByName('sNombre').AsString;
             fdDetalle.FieldByName('iCantidad').Value:=qry.FieldByName('iCantidad').AsFloat;
             fdDetalle.FieldByName('iCantidadRecibida').Value:=qry.FieldByName('iCantidadRecibida').AsFloat;
             fdDetalle.FieldByName('iCantidadRecepcionada').Value:=0;
           fdDetalle.Post;
         except
          on e:Exception do
           begin
            ShowMessage('Al intentar cargar los datos se produjo el siguiente ERROR:'#10#13+e.Message);
           end;
         end;
        qry.Next;
       end;
        fdDetalle.SaveToStream(oStr,sfXML);
      for I := 0 to ReceptorColector.RemoteProfiles.Count-1 do
        if ReceptorColector.RemoteProfiles[I].ProfileIdentifier = idRemoto then
          stringReciverProfile.SendStream(ReceptorColector.RemoteProfiles[I],
            'Informa detalle',oStr);
    finally
      qry.Close();
    end;
  except
   on e:exception do
    CargarError(e.Message);
  end;
end;

procedure TFmReceptorColector.CargarRecepcionMercaderia(idRemoto: string);
var
  precio, nombre: String;
  I: Integer;
  st:TStringList;
  res:string;
  //Campos
  oStr: TStringStream;
begin
  try
    res:='';
    oStr:=TStringStream.Create;
    qry.sql.text := 'EXEC getRecepcionMercaderiaCabecera ';
    try
      qry.Open();
      qry.First;
      fdCabecera.Open;
      fdCabecera.EmptyDataSet;
      while not qry.eof do
       begin
         try
           fdCabecera.Append;
             fdCabecera.FieldByName('sNumDoc').Value:=qry.FieldByName('sNumDoc').AsString;
             fdCabecera.FieldByName('fDocumento').Value:=qry.FieldByName('fDocumento').AsDateTime;
             fdCabecera.FieldByName('idTipoDoc').Value:=qry.FieldByName('idTipoDoc').AsString;
             fdCabecera.FieldByName('idSucursal').Value:=qry.FieldByName('idSucursal').AsString;
             fdCabecera.FieldByName('idProveedor').Value:=qry.FieldByName('idProveedor').AsString;
             fdCabecera.FieldByName('idNumDoc').Value:=qry.FieldByName('idNumDoc').AsString;
             fdCabecera.FieldByName('sRazonSocial').Value:=qry.FieldByName('sRazonSocial').AsString;
             fdCabecera.FieldByName('fechafloat').Value:=qry.FieldByName('fDocumento').AsFloat;
           fdCabecera.Post;
         except
          on e:Exception do
           begin
            ShowMessage('Al intentar cargar los datos se produjo el siguiente ERROR:'#10#13+e.Message);
           end;
         end;
        qry.Next;
       end;
         fdCabecera.SaveToStream(oStr,sfXML);
      for I := 0 to ReceptorColector.RemoteProfiles.Count-1 do
        if ReceptorColector.RemoteProfiles[I].ProfileIdentifier = idRemoto then
          stringReciverProfile.SendStream(ReceptorColector.RemoteProfiles[I],
            'Informa cabecera',oStr);
    finally
      qry.Close();
    end;
  except
   on e:exception do
    CargarError(e.Message);
  end;
end;


procedure TFmReceptorColector.CargarProveedores(idRemoto: string);
var
  precio, nombre: String;
  I: Integer;
  st:TStringList;
  res:string;
  //Campos
  oStr: TStringStream;
begin
  try
    res:='';
    oStr:=TStringStream.Create;
    qry.sql.text := ' EXEC getProveedoresColector ';
    try
      qry.Open();
      qry.First;
      fdProveedores.Open;
      fdProveedores.EmptyDataSet;

        fdProveedores.Append;
         fdProveedores.FieldByName('idProveedor').Value:=-1;
         fdProveedores.FieldByName('sRazonSocial').Value:='<Sin Filtrar>';
        fdProveedores.Post;

      while not qry.eof do
       begin
         try
          fdProveedores.Append;
           fdProveedores.FieldByName('idProveedor').Value:=qry.FieldByName('idProveedor').AsInteger;
           fdProveedores.FieldByName('sRazonSocial').Value:=qry.FieldByName('sRazonSocial').AsString;
          fdProveedores.Post;
         except
          on e:Exception do
           begin
            ShowMessage('Al intentar cargar los datos se produjo el siguiente ERROR:'#10#13+e.Message);
           end;
         end;
        qry.Next;
       end;
      fdProveedores.SaveToStream(oStr,sfXML);
      for I := 0 to ReceptorColector.RemoteProfiles.Count-1 do
        if ReceptorColector.RemoteProfiles[I].ProfileIdentifier = idRemoto then
          stringReciverProfile.SendStream(ReceptorColector.RemoteProfiles[I],
            'Informa proveedores',oStr);
    finally
      qry.Close();
    end;
  except
   on e:exception do
    CargarError(e.Message);
  end;
end;

procedure TFmReceptorColector.CargarRecepcionFiltro1(idRemoto: string);
var
 oStr: TStringStream;
 i:Integer;
begin
 try
   try
    oStr:=TStringStream.Create;
    qryRecepcionFiltro1.Close;
    qryRecepcionFiltro1.sql.text := ' EXEC getRecepcionFiltro1Colector ';
    qryRecepcionFiltro1.Open();
    qryRecepcionFiltro1.SaveToStream(oStr,sfXML);
    for I := 0 to ReceptorColector.RemoteProfiles.Count-1 do
      if ReceptorColector.RemoteProfiles[I].ProfileIdentifier = idRemoto then
        stringReciverProfile.SendStream(ReceptorColector.RemoteProfiles[I],
          'Informa RecepcionFiltro1',oStr);

   finally
    qryRecepcionFiltro1.Close;
   end;
 except
  on e:exception do
   CargarError(e.Message);
 end;
end;

procedure TFmReceptorColector.CargarRecepcionFiltro2(idRemoto: string; ID_F1: string);
var
 oStr: TStringStream;
 i:Integer;
begin
 try
   try
    oStr:=TStringStream.Create;
    qryRecepcionFiltro2.Close;
    qryRecepcionFiltro2.sql.text := ' EXEC getRecepcionFiltro2Colector '+ID_F1;
    qryRecepcionFiltro2.Open();
    qryRecepcionFiltro2.SaveToStream(oStr,sfXML);
    for I := 0 to ReceptorColector.RemoteProfiles.Count-1 do
      if ReceptorColector.RemoteProfiles[I].ProfileIdentifier = idRemoto then
        stringReciverProfile.SendStream(ReceptorColector.RemoteProfiles[I],
          'Informa RecepcionFiltro2',oStr);

   finally
    qryRecepcionFiltro2.Close;
   end;
 except
  on e:exception do
   CargarError(e.Message);
 end;
end;

procedure TFmReceptorColector.CargarRecepcionFiltro3(idRemoto: string; ID_F1, ID_F2: string);
var
 oStr: TStringStream;
 i:Integer;
begin
 try
   try
    oStr:=TStringStream.Create;
    qryRecepcionFiltro3.Close;
    qryRecepcionFiltro3.sql.text := ' EXEC getRecepcionFiltro3Colector '+ID_F1+', '+ID_F2;
    qryRecepcionFiltro3.Open();
    qryRecepcionFiltro3.SaveToStream(oStr,sfXML);
    for I := 0 to ReceptorColector.RemoteProfiles.Count-1 do
      if ReceptorColector.RemoteProfiles[I].ProfileIdentifier = idRemoto then
        stringReciverProfile.SendStream(ReceptorColector.RemoteProfiles[I],
          'Informa RecepcionFiltro3',oStr);
   finally
    qryRecepcionFiltro3.Close;
   end;
 except
  on e:exception do
   CargarError(e.Message);
 end;
end;

procedure TFmReceptorColector.stringReciverProfileResourceReceived(
  const Sender: TObject; const AResource: TRemoteResource);
var
  a: String;
  Hint: TStringList;
  res: TStringList;
  idTipoDoc, identificadorRemoto, accion, filtro1, filtro2: string;
  idProveedor, idSucursal ,idNumDoc: Integer;
begin
  try
    Hint := TStringList.Create();
    res := TStringList.Create();
    split(',', AResource.Value.AsString, res);
    split(',', AResource.Hint, Hint);
    identificadorRemoto := Hint[0];
    accion := Hint[1];

    if accion = 'Informar codigo' then
     begin
      CargarListBox(res);
      CargarEnBD(identificadorRemoto, res);
     end
    else
    if accion = 'Test informar codigo' then
      CargarListBox(res)
    else
    if accion = 'Consultar codigo' then
      ConsultarCodigo(identificadorRemoto, res[0])
    else
    if accion = 'Recepcion mercaderia cabecera' then
      CargarRecepcionMercaderia(identificadorRemoto)
    else
    if accion = 'Recepcion mercaderia detalle' then
     begin
      idTipoDoc:=Hint[2];
      idProveedor:=StrToInt(Hint[3]);
      idSucursal:=StrToInt(Hint[4]);
      idNumDoc:=StrToInt(Hint[5]);
      CargarRecepcionMercaderiaDetalle(identificadorRemoto,idTipoDoc,idProveedor,idSucursal,idNumDoc);
     end
    else
    if accion = 'Recepcion mercaderia Filtro1' then
     begin
      CargarRecepcionFiltro1(identificadorRemoto);
     end
    else
    if accion = 'Recepcion mercaderia Filtro2' then
     begin
      filtro1:=Hint[2];
      CargarRecepcionFiltro2(identificadorRemoto,filtro1);
     end
    else
    if accion = 'Recepcion mercaderia Filtro3' then
     begin
      filtro1:=Hint[2];
      filtro2:=Hint[3];
      CargarRecepcionFiltro3(identificadorRemoto,filtro1,filtro2);
     end
    else
    if accion = 'Informar recepcion mercaderia' then
     CargarRecepcionEnBD(identificadorRemoto,TStringStream(AResource.Value.AsStream))
    else
    if accion = 'Informar recepcion mercaderia sin OC' then
     CargarRecepcionSinOCEnBD(identificadorRemoto,TStringStream(AResource.Value.AsStream))
    else
    if accion = 'Listado proveedores' then
     CargarProveedores(identificadorRemoto)
    else
     ShowMessage('NN.');

  except
   on e:exception do
    CargarError(e.Message);
  end;
end;

procedure TFmReceptorColector.btnLoginClick(Sender: TObject);
begin
  lbCodigos.Clear;
end;

procedure TFmReceptorColector.CargarError(err: string);
var
 res: TStringList;
 Item: TListBoxItem;
 importe: Double;
 a: string;
begin
 try
    Item := TListBoxItem.Create(nil);
    Item.Text := err;
    Item.StyledSettings := Item.StyledSettings - [TStyledSetting.Other];
    Item.TextSettings.WordWrap := true;
    Item.Parent := lbCodigos;
    //Item.TagString := res[0];
    Item.StyleLookup := 'LbCodigosStyle';
    //Item.StylesData['importe'] := Format('$ %.2f', [importe]);
 except
  on e:Exception do
   begin

   end;
 end;
end;

procedure TFmReceptorColector.CargarListBox(res: TStringList);
var
  Item: TListBoxItem;
  importe: Double;
  a: string;
begin
 try
  try
    a := StringReplace(res[3], '''', '', [rfReplaceAll]);
    importe := StrToFloat(a);
  except
    importe := 0.0;
  end;
  Item := TListBoxItem.Create(nil);
  Item.Text := res[4];
  Item.Parent := lbCodigos;
//    Item.TextSettings.FontColor := '';
//    Item.TextSettings.Font.Size := '';
//    Item.StyledSettings := Item.StyledSettings - [TStyledSetting.FontColor, TStyledSetting.Size];
  Item.TagString := res[0];
  Item.StyleLookup := 'LbCodigosStyle';
  Item.StylesData['importe'] := Format('$ %.2f', [importe]);
  Item.StyledSettings := Item.StyledSettings - [TStyledSetting.Other];
  Item.TextSettings.WordWrap := true;
 except
  on e:exception do
   CargarError(e.Message);
 end;
end;

procedure TFmReceptorColector.ConnAfterConnect(Sender: TObject);
begin
 try
  lblBD.Text:='Sin Conexion ';
  if Conn.Connected then
  lblBD.Text:=Conn.CurrentCatalog;
 except
  on e:exception do
   CargarError(e.Message);
 end;
end;

procedure TFmReceptorColector.ConsultarCodigo(idRemoto: string; codigo: string);
var
  precio, nombre: String;
  I: Integer;
begin
  try
    if ReceptorColector.RemoteProfiles.Count<1 then
      BuscarReceptorRemoto;
    qry.sql.text := 'exec consultaprecios ' + QuotedStr(codigo);
    try
      qry.Open();
      nombre := qry.FieldByName('sNombre').AsString;
      precio := FloatToStr(qry.FieldByName('rPrecioVenta').AsFloat);
      for I := 0 to ReceptorColector.RemoteProfiles.Count-1 do
        if ReceptorColector.RemoteProfiles[I].ProfileIdentifier = idRemoto then
          stringReciverProfile.sendString(ReceptorColector.RemoteProfiles[I],
            'Informa producto', format('%s|%s', [nombre, precio]));
    finally
      qry.Close();
    end;
  except
    on e: Exception do
     CargarError(e.Message);
  end;
end;

procedure TFmReceptorColector.buscarReceptorRemoto;
var
  I: Integer;
begin
 try
  for I := ReceptorColector.PairedManagers.Count - 1 downto 0 do
    ReceptorColector.UnPairManager(ReceptorColector.PairedManagers[I]);
  ReceptorColector.DiscoverManagers;
 except
  on e:exception do
   CargarError(e.Message);
 end;
end;

procedure TFmReceptorColector.CargarEnBD(idRemoto: string; res: TStringList);
var
  id, codigo: string;
  importe: Double;
  I: Integer;
begin
  try
    id := trim(res[0]);
    codigo := trim(res[1]);
    importe := strToFloat(res[3]);
    Conn.ExecSQL('INSERT INTO TMPPreciosColectora (id, sCodigoBarra, fToma, rImporte) ' +
                  'VALUES (:id, :sCodigo, :fToma, :rImporte)', [
                  res[0], res[1], gDateToStr(now), res[3]]);
    for I := 0 to ReceptorColector.RemoteProfiles.Count-1 do
        if ReceptorColector.RemoteProfiles[I].ProfileIdentifier = idRemoto then
          stringReciverProfile.Resources.FindByName('codigoInformado').Value := res[1];
    res.Free;
  except
   on e:exception do
    CargarError(e.Message);
  end;
end;


procedure TFmReceptorColector.CargarRecepcionSinOCEnBD(idRemoto: string; res: TStringStream);
var
  sQ: string;
  importe: Double;
  I:Integer;
begin
  try
   fdRecepcionDetalle.LoadFromStream(res,sfXML);
   if not fdRecepcionDetalle.IsEmpty then
    begin
      fdRecepcionDetalle.First;
      while not fdRecepcionDetalle.Eof do
       begin
          sQ:=' INSERT INTO TMPRecepcionesColectora '+
              ' (NumRecepcion,Id,Cantidad) VALUES ('+
              QuotedStr(fdRecepcionDetalle.FieldByName('NumRecepcion').AsString)+', ' +
              QuotedStr(fdRecepcionDetalle.FieldByName('Id').AsString)+', '+
              FloatToStr(fdRecepcionDetalle.FieldByName('Cantidad').AsFloat)+') ';
           Conn.ExecSQL(sQ);
         fdRecepcionDetalle.Next;
       end;
    end;

    for I := 0 to ReceptorColector.RemoteProfiles.Count-1 do
      if ReceptorColector.RemoteProfiles[I].ProfileIdentifier = idRemoto then
        stringReciverProfile.sendString(ReceptorColector.RemoteProfiles[I],
          'Informa recepcion mercaderia sin OC', 'OK');
  except
    on e: Exception do
     begin
      for I := 0 to ReceptorColector.RemoteProfiles.Count-1 do
        if ReceptorColector.RemoteProfiles[I].ProfileIdentifier = idRemoto then
          stringReciverProfile.sendString(ReceptorColector.RemoteProfiles[I],
            'Informa recepcion mercaderia sin OC', e.message);
      CargarError(e.Message);
     end;
  end;
end;

procedure TFmReceptorColector.CargarRecepcionEnBD(idRemoto: string; res: TStringStream);
var
  sQ: string;
  importe: Double;
  I:Integer;
begin
  try
     fdDetalle.LoadFromStream(res,sfXML);
     if not fdDetalle.IsEmpty then
      begin
        fdDetalle.First;
        while not fdDetalle.Eof do
         begin
            sQ:=' INSERT INTO TMPComprasColectora '+
                ' (idTipoDoc,idProveedor,idSucursal,idNumDoc,idNumLinea,idProducto,rCantidadRecepcionada) VALUES ('+
                QuotedStr(fdDetalle.FieldByName('idTipoDoc').AsString)+', ' +
                IntToStr(fdDetalle.FieldByName('idProveedor').AsInteger)+', '+
                IntToStr(fdDetalle.FieldByName('idSucursal').AsInteger)+', '+
                IntToStr(fdDetalle.FieldByName('idNumDoc').AsInteger)+', '+
                IntToStr(fdDetalle.FieldByName('idNumLinea').AsInteger)+', '+
                IntToStr(fdDetalle.FieldByName('idProducto').AsInteger)+', '+
                FloatToStr(fdDetalle.FieldByName('iCantidadRecepcionada').AsFloat)+') ';
             Conn.ExecSQL(sQ);
           fdDetalle.Next;
         end;
      end;
      sQ:='EXEC SP_ProcesarComprasColectores ';
      Conn.ExecSQL(sQ);

      for I := 0 to ReceptorColector.RemoteProfiles.Count-1 do
        if ReceptorColector.RemoteProfiles[I].ProfileIdentifier = idRemoto then
          stringReciverProfile.sendString(ReceptorColector.RemoteProfiles[I],
            'Informa recepcion mercaderia', 'OK');
  except
    on e: Exception do
     begin
      for I := 0 to ReceptorColector.RemoteProfiles.Count-1 do
        if ReceptorColector.RemoteProfiles[I].ProfileIdentifier = idRemoto then
          stringReciverProfile.sendString(ReceptorColector.RemoteProfiles[I],
            'Informa recepcion mercaderia', e.message);
      CargarError(e.Message);
     end;
  end;
end;

procedure TFmReceptorColector.AgregarConexion(cant: integer);
begin
  try
    _conexiones := _conexiones + cant;
    if _conexiones>0 then
    begin
      lblCantConexiones.Text := IntToStr(_conexiones)+' conexiones';
      cirCantConexiones.Fill.Color := $FF1C9D43;
    end
    else
    begin
      lblCantConexiones.Text := '0 conexiones';
      cirCantConexiones.Fill.Color := $FFD8473E;
    end;
  except
   on e:exception do
    CargarError(e.Message);
  end;
end;

end.
