unit uPrincipal;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  FMX.Controls.Presentation, FMX.Objects, FMX.Layouts, IPPeerClient,
  IPPeerServer, System.Tether.Manager, System.Tether.AppProfile, FMX.Ani,
  Records.Codigos, PubSub.InterfaceActions, PubSub.Interfaces,
  Globales, uScanner, uRecepcionMercaderia, FMX.MultiView, FMX.Edit,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client, FireDAC.UI.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys,
  FireDAC.FMXUI.Wait, Config, FMX.Effects;

type
  TFmPrincipal = class(TForm)
    Header: TToolBar;
    HeaderLabel: TLabel;
    btnSalir: TSpeedButton;
    SecAcciones1: TLayout;
    lblConnStatus: TLabel;
    TetheringManager: TTetheringManager;
    TetheringAppProfile: TTetheringAppProfile;
    tmrAutoConnect: TTimer;
    anim1: TFloatAnimation;
    anim2: TFloatAnimation;
    Image3: TImage;
    btnIcono: TSpeedButton;
    imgIcono: TImage;
    animIcono: TFloatAnimation;
    Contenedor: TLayout;
    opcionIp: TMultiView;
    Label1: TLabel;
    edtIP: TEdit;
    Button1: TButton;
    schAuto: TSwitch;
    Label2: TLabel;
    aniAlto: TFloatAnimation;
    btnConectarIP: TButton;
    btnConectar: TButton;
    btnScan: TButton;
    ImgScan: TImage;
    btnRecOC: TButton;
    ImgRecOC: TImage;
    btnRecSinOC: TButton;
    ImgRecSinOC: TImage;
    lblScanner: TLabel;
    lblRecSinOC: TLabel;
    lblRecOC: TLabel;
    bkBtnScan: TRectangle;
    bkBtnRecOC: TRectangle;
    bkBtnRecSinOC: TRectangle;
    StyleBook1: TStyleBook;
    background: TRectangle;
    ShadowEffect1: TShadowEffect;
    ShadowEffect2: TShadowEffect;
    ShadowEffect3: TShadowEffect;
    ShadowEffect4: TShadowEffect;
    procedure btnSalirClick(Sender: TObject);
    procedure btnScannerClick(Sender: TObject);
    procedure btnOCClick(Sender: TObject);
    procedure TetheringManagerEndManagersDiscovery(const Sender: TObject;
      const ARemoteManagers: TTetheringManagerInfoList);
    procedure TetheringManagerRequestManagerPassword(const Sender: TObject;
      const ARemoteIdentifier: string; var Password: string);
    procedure buscarReceptor;
    procedure FormShow(Sender: TObject);
    procedure TetheringManagerEndProfilesDiscovery(const Sender: TObject;
      const ARemoteProfiles: TTetheringProfileInfoList);
    procedure TetheringAppProfileDisconnect(const Sender: TObject;
      const AProfileInfo: TTetheringProfileInfo);
    procedure TetheringManagerRemoteManagerShutdown(const Sender: TObject;
      const AManagerIdentifier: string);
    procedure TetheringManagerUnPairManager(const Sender: TObject;
      const AManagerInfo: TTetheringManagerInfo);
    procedure tmrAutoConnectTimer(Sender: TObject);
    procedure anim1Finish(Sender: TObject);
    procedure anim2Finish(Sender: TObject);
    procedure TetheringAppProfileResourceReceived(const Sender: TObject;
      const AResource: TRemoteResource);
    procedure TetheringAppProfileResourceUpdated(const Sender: TObject;
      const AResource: TRemoteResource);
    procedure animIconoFinish(Sender: TObject);
    procedure Label2Click(Sender: TObject);
    procedure edtIPClick(Sender: TObject);
    procedure opcionIpHidden(Sender: TObject);
    procedure aniAltoFinish(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure ContenedorClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnConectarIPClick(Sender: TObject);
    procedure btnConectarClick(Sender: TObject);
    procedure btnScanClick(Sender: TObject);
    procedure btnRecOCClick(Sender: TObject);
    procedure btnRecSinOCClick(Sender: TObject);
  private
    { Private declarations }
    FormScanner: TFmScanner;
    FormRecepcionMercaderia: TFmRecepcionMercaderia;
    procedure testEnvio;
    procedure NotificacionNuevoCodigo(const notifyClass: INotificationClass);
    procedure EnviarCodigo(obj: TCodigoColectado);
    procedure ConsultarCodigo(codigo: string);
    procedure MostrarDesconectado();
    procedure MostrarConectando();
    procedure MostrarConectado();
    function FormatIP(ip: string): String;
    function ObtenerUltimaIPConectada(): string;
    procedure GuardarIP;
    procedure configInicioAuto;
  public
    { Public declarations }
    Connected: Boolean;
    procedure BuscraDatos(codigo: string);
    procedure ObtenerCabeceras();
    procedure ObtenerProveedores();
    procedure ObtenerRecepcionFiltro1();
    procedure ObtenerRecepcionFiltro2(ID_F1:string);
    procedure ObtenerRecepcionFiltro3(ID_F1,ID_F2:string);
    procedure InformarRecepcionMercaderia(val: TStringStream);
    procedure InformarRecepcionMercaderiaSinOC(val: TStringStream);
    procedure InformarRecepcionFiltro1(val: TStringStream);
    procedure InformarRecepcionFiltro2(val: TStringStream);
    procedure InformarRecepcionFiltro3(val: TStringStream);
    procedure ObtenerDetalle(idTipoDoc: String;idProveedor, idSucursal ,idNumDoc: Integer);
  end;

var
  FmPrincipal: TFmPrincipal;

implementation

uses
  FMX.Platform, PubSub.Subscriber, PubSub.Notifications;

{$R *.fmx}

procedure TFmPrincipal.aniAltoFinish(Sender: TObject);
begin
  aniAlto.Enabled := false;
end;

procedure TFmPrincipal.anim1Finish(Sender: TObject);
begin
  if anim1.Enabled then
  begin
    anim2.Enabled := true;
    anim1.Enabled := false;
  end;
end;

procedure TFmPrincipal.FormShow(Sender: TObject);
begin
  opcionIp.Size.Height := 150;
  MostrarDesconectado;
//  TetheringManager.AutoConnect(3000);
  edtIP.Text := ObtenerUltimaIPConectada;
  configInicioAuto;
  buscarReceptor;

end;

procedure TFmPrincipal.MostrarDesconectado();
begin
  btnRecOC.Enabled:=False;
  btnRecSinOC.Enabled:=False;
  btnScan.Enabled:=False;
  Connected := false;
  lblConnStatus.FontColor := $FFEDFFFF;
  lblConnStatus.Text := 'Desconectado';
  lblConnStatus.Visible := true;
  anim1.Enabled := false;
  anim2.Enabled := false;
  lblConnStatus.Opacity := 1;
end;

procedure TFmPrincipal.MostrarConectando();
begin
  Connected := false;
  lblConnStatus.FontColor := $FFEDFFFF;
  lblConnStatus.Text := 'Conectando...';
  lblConnStatus.Opacity := 1;
  anim1.Enabled := true;
  anim2.Enabled := false;
end;

procedure TFmPrincipal.MostrarConectado();
begin
  btnRecOC.Enabled:=True;
  btnRecSinOC.Enabled:=True;
  btnScan.Enabled:=True;
  lblConnStatus.FontColor := $FFEDFFFF;//$FF13FF44;
  lblConnStatus.Text := 'Conectado';
  lblConnStatus.Visible := true;
  anim1.Enabled := false;
  anim2.Enabled := false;
  lblConnStatus.Opacity := 1;
end;

procedure TFmPrincipal.Label2Click(Sender: TObject);
begin
  schAuto.IsChecked := not schAuto.IsChecked;
end;

procedure TFmPrincipal.btnSalirClick(Sender: TObject);
var
  ASyncService: IFMXDialogServiceASync;
begin
  if TPlatformServices.Current.SupportsPlatformService
                (IFMXDialogServiceASync, IInterface(ASyncService)) then
  begin
    if Self.Visible then
      ASyncService.MessageDialogAsync(DLG_CERRAR,
        TMsgDlgType.mtInformation, [System.UITypes.TMsgDlgBtn.mbYes,
        System.UITypes.TMsgDlgBtn.mbNo], TMsgDlgBtn.mbNo, 0,
        procedure(const AResult: TModalResult)
        begin
          case AResult of
            mrYes:
              begin
                Application.Terminate;
              end;
          end;
        end);
  end;
end;

procedure TFmPrincipal.btnScanClick(Sender: TObject);
var
  suscNuevoCodigo: ISubscriberInterface;
begin
  // Suscribo a eventos lanzados por el form
  suscNuevoCodigo := TPubSubSubscriber.Create;
  suscNuevoCodigo.SetUpdateSubscriberMethod(NotificacionNuevoCodigo);
  FormScanner := TFmScanner.Create(nil);
  FormScanner.Provider.Subscribe(suscNuevoCodigo);
  FormScanner.Show();
end;

procedure TFmPrincipal.btnScannerClick(Sender: TObject);
var
  suscNuevoCodigo: ISubscriberInterface;
begin
  // Suscribo a eventos lanzados por el form
  suscNuevoCodigo := TPubSubSubscriber.Create;
  suscNuevoCodigo.SetUpdateSubscriberMethod(NotificacionNuevoCodigo);
  FormScanner := TFmScanner.Create(nil);
  FormScanner.Provider.Subscribe(suscNuevoCodigo);
  FormScanner.Show();
end;

procedure TFmPrincipal.btnConectarClick(Sender: TObject);
begin
  animIcono.Enabled := true;
  buscarReceptor;
end;

procedure TFmPrincipal.btnConectarIPClick(Sender: TObject);
begin
  opcionIp.ShowMaster;
end;

procedure TFmPrincipal.btnOCClick(Sender: TObject);
//var
//  suscNuevoCodigo: ISubscriberInterface;
begin
  // Suscribo a eventos lanzados por el form
//  suscNuevoCodigo := TPubSubSubscriber.Create;
//  suscNuevoCodigo.SetUpdateSubscriberMethod(NotificacionNuevoCodigo);
  FormRecepcionMercaderia := TFmRecepcionMercaderia.Create(nil);
  //FormRecepcionMercaderia.Provider.Subscribe(suscNuevoCodigo);
  FormRecepcionMercaderia.Show();

  //testEnvio;
end;

procedure TFmPrincipal.btnRecOCClick(Sender: TObject);
begin
  FormRecepcionMercaderia := TFmRecepcionMercaderia.Create(nil);
  FormRecepcionMercaderia.ShowModalParam(1);
end;

procedure TFmPrincipal.btnRecSinOCClick(Sender: TObject);
begin
  FormRecepcionMercaderia := TFmRecepcionMercaderia.Create(nil);
  FormRecepcionMercaderia.ShowModalParam(2);
end;

procedure TFmPrincipal.TetheringAppProfileDisconnect(const Sender: TObject;
  const AProfileInfo: TTetheringProfileInfo);
begin
  MostrarDesconectado;
end;

// Recibimos comunicacion del servidor con datos del
// producto solicitado.
procedure TFmPrincipal.TetheringAppProfileResourceReceived(
  const Sender: TObject; const AResource: TRemoteResource);
begin
 try
  if AResource.Hint = 'Informa producto' then
    if Assigned(FormScanner) then
      FormScanner.RecibirComunicacionProducto(AResource.Value.AsString);

  if AResource.Hint = 'Informa cabecera' then
    if Assigned(FormRecepcionMercaderia) then
      FormRecepcionMercaderia.RecibirCabecera(TStringStream(AResource.Value.AsStream));

  if AResource.Hint = 'Informa detalle' then
    if Assigned(FormRecepcionMercaderia) then
      FormRecepcionMercaderia.RecibirDetalle(TStringStream(AResource.Value.AsStream));

  if AResource.Hint = 'Informa proveedores' then
    if Assigned(FormRecepcionMercaderia) then
      FormRecepcionMercaderia.RecibirProveedores(TStringStream(AResource.Value.AsStream));

  if AResource.Hint = 'Informa recepcion mercaderia' then
    if Assigned(FormRecepcionMercaderia) then
      FormRecepcionMercaderia.ConfirmarProcesoCompras(AResource.Value.AsString);

  if AResource.Hint = 'Informa recepcion mercaderia sin OC' then
    if Assigned(FormRecepcionMercaderia) then
      FormRecepcionMercaderia.ConfirmarProcesoRecepcion(AResource.Value.AsString);

  if AResource.Hint = 'Informa RecepcionFiltro1' then
    if Assigned(FormRecepcionMercaderia) then
      FormRecepcionMercaderia.RecibirRecepcionFiltro1(TStringStream(AResource.Value.AsStream));

  if AResource.Hint = 'Informa RecepcionFiltro2' then
    if Assigned(FormRecepcionMercaderia) then
      FormRecepcionMercaderia.RecibirRecepcionFiltro2(TStringStream(AResource.Value.AsStream));

  if AResource.Hint = 'Informa RecepcionFiltro3' then
    if Assigned(FormRecepcionMercaderia) then
      FormRecepcionMercaderia.RecibirRecepcionFiltro3(TStringStream(AResource.Value.AsStream));
 except
  on e:Exception do
   begin
    ShowMessage('Error: '+e.Message);
   end;
 end;
end;

// Se ejecuta cuando cambia el valor de un recurso remoto
// al cual estamos suscriptos.
procedure TFmPrincipal.TetheringAppProfileResourceUpdated(const Sender: TObject;
  const AResource: TRemoteResource);
begin
  if Assigned(FormScanner) then
    FormScanner.ConfirmarCodigoInformado(AResource.Value.AsString);
end;

procedure TFmPrincipal.TetheringManagerEndManagersDiscovery(
  const Sender: TObject; const ARemoteManagers: TTetheringManagerInfoList);
var
  I: Integer;
  seEmparejo: boolean;
begin
  seEmparejo := false;
  for I := 0 to ARemoteManagers.Count - 1 do
    if (ARemoteManagers[I].ManagerText = 'ReceptorColector') then
    begin
      try
        TetheringManager.PairManager(ARemoteManagers[I]);
        seEmparejo := true;
      except
      end;
    end;

  imgIcono.Enabled := true;

  if not seEmparejo then
  begin
    MostrarDesconectado;
    tmrAutoConnect.Enabled := true;
  end;
end;

procedure TFmPrincipal.TetheringManagerEndProfilesDiscovery(
  const Sender: TObject; const ARemoteProfiles: TTetheringProfileInfoList);
var
  I: Integer;
begin
  for I := 0 to TetheringManager.RemoteProfiles.Count - 1 do
    if (TetheringManager.RemoteProfiles[I].ProfileText = 'StringReciverApp') then
      Connected := TetheringAppProfile.Connect(TetheringManager.RemoteProfiles[0]);

  if Connected then
  begin
    MostrarConectado;
    GuardarIP;
    TetheringAppProfile.SubscribeToRemoteItem(TetheringManager.RemoteProfiles[0],
      'codigoInformado');
  end
  else
  begin
    MostrarDesconectado;
  end;
  tmrAutoConnect.Enabled := true;
end;

procedure TFmPrincipal.TetheringManagerRemoteManagerShutdown(
  const Sender: TObject; const AManagerIdentifier: string);
begin
  MostrarDesconectado;
end;

procedure TFmPrincipal.TetheringManagerRequestManagerPassword(
  const Sender: TObject; const ARemoteIdentifier: string; var Password: string);
begin
  Password := '1234';
end;

procedure TFmPrincipal.TetheringManagerUnPairManager(const Sender: TObject;
  const AManagerInfo: TTetheringManagerInfo);
begin
  MostrarDesconectado;
end;

procedure TFmPrincipal.anim2Finish(Sender: TObject);
begin
  if anim2.Enabled then
  begin
    anim1.Enabled := true;
    anim2.Enabled := false;
  end;
end;

procedure TFmPrincipal.animIconoFinish(Sender: TObject);
begin
  animIcono.Enabled := false;
  imgIcono.Enabled := false;
end;

procedure TFmPrincipal.tmrAutoConnectTimer(Sender: TObject);
begin
  if not Connected then
  begin
    MostrarDesconectado;
    if schAuto.IsChecked then
    begin
      tmrAutoConnect.Enabled := false;
      buscarReceptor;
    end;
  end;
end;

procedure TFmPrincipal.buscarReceptor;
var
  I: Integer;
  sIP: string;
begin
  TetheringManager.CancelDiscoverManagers;
  MostrarConectando;
  for I := TetheringManager.PairedManagers.Count - 1 downto 0 do
    TetheringManager.UnPairManager(TetheringManager.PairedManagers[I]);
  sIP := FormatIP(edtIP.Text);
  try
    if sIP<>'' then
      TetheringManager.DiscoverManagers(3000, sIP)
    else
      TetheringManager.DiscoverManagers(3000);
  except
  end;
end;

procedure TFmPrincipal.testEnvio;
var
  i: integer;
begin
  if not Connected then
  begin
    if TetheringManager.RemoteProfiles.Count < 1 then
      buscarReceptor;
    Connected := TetheringAppProfile.Connect(TetheringManager.RemoteProfiles[0]);
  end;
  try
    TetheringAppProfile.SendString(TetheringManager.RemoteProfiles[0],
      TetheringAppProfile.Identifier+',Test informar codigo',
      'aa1234456bc456c78889fff6898765ff,77456787554432,' +
      '18/02/20 09:00,23.55,El nombre del producto');
  finally

  end;
end;

procedure TFmPrincipal.edtIPClick(Sender: TObject);
begin
  //opcionIp.Size.Height := 360;
  aniAlto.Enabled := true;
end;

procedure TFmPrincipal.EnviarCodigo(obj: TCodigoColectado);
var
  i: integer;
begin
  if not Connected then
  begin
    if TetheringManager.RemoteProfiles.Count < 1 then
      buscarReceptor;
    Connected := TetheringAppProfile.Connect(TetheringManager.RemoteProfiles[0]);
  end;

  try
    TetheringAppProfile.SendString(TetheringManager.RemoteProfiles[0],
      TetheringAppProfile.Identifier+',Informar codigo',
      Format('%s,%s,%s,%s,%s', [obj.id, obj.codigo, obj.fecha,
      FloatToStr(obj.precio), obj.nombre]));
  finally

  end;
end;

procedure TFmPrincipal.ConsultarCodigo(codigo: string);
var
  i: integer;
  a: String;
begin
  if not Connected then
  begin
    if TetheringManager.RemoteProfiles.Count < 1 then
      buscarReceptor;
    Connected := TetheringAppProfile.Connect(TetheringManager.RemoteProfiles[0]);
  end;
  try
    a := TetheringAppProfile.Identifier;
    TetheringAppProfile.SendString(TetheringManager.RemoteProfiles[0],
      TetheringAppProfile.Identifier+',Consultar codigo', codigo);
  finally

  end;
end;

procedure TFmPrincipal.ContenedorClick(Sender: TObject);
begin

end;

// Aca llegan las Notificaciones o Eventos del otro form (uScanner)
procedure TFmPrincipal.NotificacionNuevoCodigo(const notifyClass: INotificationClass);
var
  tmpNotifClass: TNotificationCodigoClass;
begin
  if notifyClass is TNotificationCodigoClass then
  begin
    tmpNotifClass:=notifyClass as TNotificationCodigoClass;
    if actNuevoCodigo in tmpNotifClass.Actions then
    begin
      EnviarCodigo(tmpNotifClass.ActionValue);
    end;
  end;
end;

procedure TFmPrincipal.opcionIpHidden(Sender: TObject);
begin
  opcionIp.Size.Height := 140;
end;

procedure TFmPrincipal.ObtenerCabeceras();
var
  i: integer;
  a: String;
begin
 try
    if not Connected then
    begin
      if TetheringManager.RemoteProfiles.Count < 1 then
        buscarReceptor;
      Connected := TetheringAppProfile.Connect(TetheringManager.RemoteProfiles[0]);
    end;
    try
      a := TetheringAppProfile.Identifier;
      TetheringAppProfile.SendString(TetheringManager.RemoteProfiles[0],
        TetheringAppProfile.Identifier+',Recepcion mercaderia cabecera', a);
    finally

    end;
 except
  on e:exception do
   begin
    ShowMessage('Error: '+e.Message);
   end;
 end;
end;

procedure TFmPrincipal.ObtenerProveedores();
var
  i: integer;
  a: String;
begin
 try
    if not Connected then
    begin
      if TetheringManager.RemoteProfiles.Count < 1 then
        buscarReceptor;
      Connected := TetheringAppProfile.Connect(TetheringManager.RemoteProfiles[0]);
    end;
    try
      a := TetheringAppProfile.Identifier;
      TetheringAppProfile.SendString(TetheringManager.RemoteProfiles[0],
        TetheringAppProfile.Identifier+',Listado proveedores', a);
    finally

    end;
 except
  on e:exception do
   begin
    ShowMessage('Error: '+e.Message);
   end;
 end;
end;

procedure TFmPrincipal.ObtenerRecepcionFiltro1();
var
  i: integer;
  a: String;
begin
 try
    if not Connected then
    begin
      if TetheringManager.RemoteProfiles.Count < 1 then
        buscarReceptor;
      Connected := TetheringAppProfile.Connect(TetheringManager.RemoteProfiles[0]);
    end;
    try
      a := TetheringAppProfile.Identifier;
      TetheringAppProfile.SendString(TetheringManager.RemoteProfiles[0],
        TetheringAppProfile.Identifier+',Recepcion mercaderia Filtro1', a);
    finally

    end;
 except
  on e:exception do
   begin
    ShowMessage('Error: '+e.Message);
   end;
 end;
end;


procedure TFmPrincipal.ObtenerRecepcionFiltro2(ID_F1:string);
var
  i: integer;
  a: String;
begin
 try
    if not Connected then
    begin
      if TetheringManager.RemoteProfiles.Count < 1 then
        buscarReceptor;
      Connected := TetheringAppProfile.Connect(TetheringManager.RemoteProfiles[0]);
    end;
    try
      a := TetheringAppProfile.Identifier;
      TetheringAppProfile.SendString(TetheringManager.RemoteProfiles[0],
        TetheringAppProfile.Identifier+',Recepcion mercaderia Filtro2,'+ID_F1, a);
    finally

    end;
 except
  on e:exception do
   begin
    ShowMessage('Error: '+e.Message);
   end;
 end;
end;

procedure TFmPrincipal.ObtenerRecepcionFiltro3(ID_F1,ID_F2:string);
var
  i: integer;
  a: String;
begin
 try
    if not Connected then
    begin
      if TetheringManager.RemoteProfiles.Count < 1 then
        buscarReceptor;
      Connected := TetheringAppProfile.Connect(TetheringManager.RemoteProfiles[0]);
    end;
    try
      a := TetheringAppProfile.Identifier;
      TetheringAppProfile.SendString(TetheringManager.RemoteProfiles[0],
        TetheringAppProfile.Identifier+',Recepcion mercaderia Filtro3,'+ID_F1+','+ID_F2, a);
    finally

    end;
 except
  on e:exception do
   begin
    ShowMessage('Error: '+e.Message);
   end;
 end;
end;

procedure TFmPrincipal.InformarRecepcionFiltro1(val: TStringStream);
var
  i: integer;
  a: String;
begin
  try
    if not Connected then
    begin
      if TetheringManager.RemoteProfiles.Count < 1 then
        buscarReceptor;
      Connected := TetheringAppProfile.Connect(TetheringManager.RemoteProfiles[0]);
    end;
    try
      a := TetheringAppProfile.Identifier;
      TetheringAppProfile.SendStream(TetheringManager.RemoteProfiles[0],
        TetheringAppProfile.Identifier+',Informa RecepcionFiltro1', val);
    finally

    end;
  except
   on e:exception do
    begin
      ShowMessage(e.Message);
    end;
  end;
end;

procedure TFmPrincipal.InformarRecepcionFiltro2(val: TStringStream);
var
  i: integer;
  a: String;
begin
  try
    if not Connected then
    begin
      if TetheringManager.RemoteProfiles.Count < 1 then
        buscarReceptor;
      Connected := TetheringAppProfile.Connect(TetheringManager.RemoteProfiles[0]);
    end;
    try
      a := TetheringAppProfile.Identifier;
      TetheringAppProfile.SendStream(TetheringManager.RemoteProfiles[0],
        TetheringAppProfile.Identifier+',Informa RecepcionFiltro2', val);
    finally

    end;
  except
   on e:exception do
    begin
      ShowMessage(e.Message);
    end;
  end;
end;

procedure TFmPrincipal.InformarRecepcionFiltro3(val: TStringStream);
var
  i: integer;
  a: String;
begin
  try
    if not Connected then
    begin
      if TetheringManager.RemoteProfiles.Count < 1 then
        buscarReceptor;
      Connected := TetheringAppProfile.Connect(TetheringManager.RemoteProfiles[0]);
    end;
    try
      a := TetheringAppProfile.Identifier;
      TetheringAppProfile.SendStream(TetheringManager.RemoteProfiles[0],
        TetheringAppProfile.Identifier+',Informa RecepcionFiltro3', val);
    finally

    end;
  except
   on e:exception do
    begin
      ShowMessage(e.Message);
    end;
  end;
end;


procedure TFmPrincipal.InformarRecepcionMercaderia(val: TStringStream);
var
  i: integer;
  a: String;
begin
  try
    if not Connected then
    begin
      if TetheringManager.RemoteProfiles.Count < 1 then
        buscarReceptor;
      Connected := TetheringAppProfile.Connect(TetheringManager.RemoteProfiles[0]);
    end;
    try
      a := TetheringAppProfile.Identifier;
      TetheringAppProfile.SendStream(TetheringManager.RemoteProfiles[0],
        TetheringAppProfile.Identifier+',Informar recepcion mercaderia', val);
    finally

    end;
  except
   on e:exception do
    begin
      ShowMessage(e.Message);
    end;
  end;
end;


procedure TFmPrincipal.InformarRecepcionMercaderiaSinOC(val: TStringStream);
var
  i: integer;
  a: String;
begin
  try
    if not Connected then
    begin
      if TetheringManager.RemoteProfiles.Count < 1 then
        buscarReceptor;
      Connected := TetheringAppProfile.Connect(TetheringManager.RemoteProfiles[0]);
    end;
    try
      a := TetheringAppProfile.Identifier;
      TetheringAppProfile.SendStream(TetheringManager.RemoteProfiles[0],
        TetheringAppProfile.Identifier+',Informar recepcion mercaderia sin OC', val);
    finally

    end;
  except
   on e:exception do
    begin
      ShowMessage(e.Message);
    end;
  end;
end;

procedure TFmPrincipal.ObtenerDetalle(idTipoDoc: String;idProveedor, idSucursal ,idNumDoc: Integer);
var
  i: integer;
  a: String;

begin
 try
    if not Connected then
    begin
      if TetheringManager.RemoteProfiles.Count < 1 then
        buscarReceptor;
      Connected := TetheringAppProfile.Connect(TetheringManager.RemoteProfiles[0]);
    end;
    try
      a := TetheringAppProfile.Identifier;
      TetheringAppProfile.SendString(TetheringManager.RemoteProfiles[0],
        TetheringAppProfile.Identifier+',Recepcion mercaderia detalle,'+idTipoDoc+','+IntToStr(idProveedor)+','+IntToStr(idSucursal)+','+IntToStr(idNumDoc), a);
    finally

    end;
 except
  on e:exception do
   begin
    ShowMessage('Error: '+e.Message);
   end;
 end;
end;

procedure TFmPrincipal.BuscraDatos(codigo: string);
begin
  ConsultarCodigo(codigo);
end;
procedure TFmPrincipal.Button1Click(Sender: TObject);
begin
  opcionIp.HideMaster;
  buscarReceptor;
end;

function TFmPrincipal.FormatIP(ip: string): String;
var
  ipSplit: TStringList;
  i: Integer;
  test: Integer;
  Res: string;
begin
  ipSplit := TStringList.Create;
  gSplit('.', ip, ipSplit);
  if ipSplit.Count=4 then
    Res := Format('%s.%s.%s.%s', [ipSplit[0], ipSplit[1], ipSplit[2], ipSplit[3]])
  else
    Res := '';
  for i := 0 to ipSplit.Count-1 do
    try
      test := StrToInt(ipSplit[i]);
      if (ipSplit[i].Length=0) or (ipSplit[i].Length>3) then
        raise Exception.Create('Error de longitud');
    except
      Res := '';
    end;

  Result := Res;
end;

procedure TFmPrincipal.FormCreate(Sender: TObject);
begin
  FormatSettings.DecimalSeparator := '.';
  FormatSettings.ThousandSeparator := ' ';
  FMX.Types.VKAutoShowMode :=TVKAutoShowMode.vkasNever;
end;

function TFmPrincipal.ObtenerUltimaIPConectada(): string;
var
  conf: TConfiguracion;
begin
  conf := TConfiguracion.Create
      (gObtenerRutaArchivoConfiguracion('user'));
  try
    Result := conf.UltimaIP;
  finally
    conf.Free;
  end;
end;

procedure TFmPrincipal.GuardarIP;
var
  conf: TConfiguracion;
begin
  if edtIP.Text = '' then
    exit;
  try
    conf := TConfiguracion.Create
      (gObtenerRutaArchivoConfiguracion('user'));
    if conf.UltimaIP <> edtIP.Text then
    begin
      conf.UltimaIP := edtIP.Text;
      conf.UpdateFile;
    end;
  finally
    conf.Free;
  end;
end;

procedure TFmPrincipal.configInicioAuto;
var
  conf: TConfiguracion;
begin
  try
    conf := TConfiguracion.Create
      (gObtenerRutaArchivoConfiguracion('user'));
    schAuto.IsChecked := false;
    if conf.ConexionAutomatica = 1 then
      schAuto.IsChecked := true;
  finally
    conf.Free;
  end;
end;


end.
