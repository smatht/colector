unit uRecepcionMercaderia;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.StdCtrls,
  FMX.Controls.Presentation, FMX.Edit, FMX.Layouts, FMX.Objects, FMX.Effects,
  PubSub.Provider, FMX.TabControl, System.Rtti, FMX.Grid.Style, FMX.Grid,
  FMX.ScrollBox, FMX.ListBox, FMX.ListView.Types, FMX.ListView.Appearances,
  FMX.ListView.Adapters.Base, FMX.ListView, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client,
  Firedac.stan.StorageXML, FireDAC.Comp.BatchMove, FireDAC.Comp.BatchMove.Text,
  System.Bindings.Outputs, Fmx.Bind.Editors, Data.Bind.EngExt,
  Fmx.Bind.DBEngExt, Data.Bind.Components, Data.Bind.DBScope,
  FireDAC.Stan.StorageBin, FMX.DateTimeCtrls, FMX.Filter.Effects;

type
  TFmRecepcionMercaderia = class(TForm)
    Header: TToolBar;
    HeaderLabel: TLabel;
    btnAtras: TSpeedButton;
    fdCabecera: TFDMemTable;
    tbcRecepcion: TTabControl;
    TCabecera: TTabItem;
    lbCabecera: TListBox;
    sbEstilos2: TStyleBook;
    aiProgreso: TAniIndicator;
    TDetalle: TTabItem;
    pnlAbajo: TPanel;
    pnlTitulo: TPanel;
    pnlCodigo: TPanel;
    pnlDetalle: TPanel;
    lbDetalle: TListBox;
    btnConfirmar: TButton;
    Rectangle2: TRectangle;
    lblNumero: TLabel;
    lblProveedor: TLabel;
    edtScan: TEdit;
    lblProducto: TLabel;
    lblCantidad: TLabel;
    edtCantidad: TEdit;
    btnAgregar: TButton;
    fdDetalle: TFDMemTable;
    fdDetalleidTipoDoc: TStringField;
    fdDetalleidSucursal: TIntegerField;
    fdDetalleidProveedor: TIntegerField;
    fdDetalleidNumDoc: TIntegerField;
    fdDetalleidNumLinea: TIntegerField;
    fdDetalleidProducto: TIntegerField;
    fdDetallesCodProducto: TStringField;
    fdDetallesCodBarra: TStringField;
    fdDetallesNombre: TStringField;
    fdDetalleiCantidad: TFloatField;
    fdDetalleiCantidadRecibida: TFloatField;
    fdDetalleiCantidadRecepcionada: TFloatField;
    pnlCabecera: TPanel;
    cbbProveedores: TComboBox;
    fdProveedores: TFDMemTable;
    fdProveedoresidProveedor: TIntegerField;
    fdProveedoressRazonSocial: TStringField;
    dtpHasta: TDateEdit;
    dtpDesde: TDateEdit;
    fdCabecerasNumDoc: TStringField;
    fdCabecerafDocumento: TDateTimeField;
    fdCabeceraidTipoDoc: TStringField;
    fdCabeceraidProveedor: TIntegerField;
    fdCabeceraidSucursal: TIntegerField;
    fdCabeceraidNumDoc: TIntegerField;
    fdCabecerasRazonSocial: TStringField;
    fdCabecerafechaFloat: TFloatField;
    Label1: TLabel;
    Label2: TLabel;
    TSinOC: TTabItem;
    pnlArriba: TPanel;
    pnlAbajo2: TPanel;
    btnConfirmar2: TButton;
    pnlCentro: TPanel;
    lbRecepcionDetalle: TListBox;
    Panel1: TPanel;
    edtScan2: TEdit;
    lblProducto2: TLabel;
    Label4: TLabel;
    edtCantidad2: TEdit;
    cbbFiltro1: TComboBox;
    cbbFiltro2: TComboBox;
    fdRecepcionFiltro1: TFDMemTable;
    fdRecepcionFiltro2: TFDMemTable;
    fdRecepcionFiltro3: TFDMemTable;
    fdRecepcionFiltro1Id: TStringField;
    fdRecepcionFiltro1Descripcion: TStringField;
    fdRecepcionFiltro2Id: TStringField;
    fdRecepcionFiltro2Descripcion: TStringField;
    fdRecepcionFiltro3Id: TStringField;
    fdRecepcionFiltro3Descripcion: TStringField;
    fdRecepcionFiltro3CodBarra: TStringField;
    edtNumeroRecepcion: TEdit;
    fdRecepcionDetalle: TFDMemTable;
    fdRecepcionDetalleNumRecepcion: TStringField;
    fdRecepcionDetalleId: TStringField;
    fdRecepcionDetalleCodBarra: TStringField;
    fdRecepcionDetalleDescripcion: TStringField;
    fdRecepcionDetalleCantidad: TFloatField;
    btnAgregar2: TButton;
    btnEliminar: TButton;
    btnTeclado: TSpeedButton;
    Layout1: TLayout;
    FillRGBEffect1: TFillRGBEffect;
    FillRGBEffect2: TFillRGBEffect;
    FillRGBEffect3: TFillRGBEffect;
    procedure btnAtrasClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure lbCabeceraItemClick(const Sender: TCustomListBox;
      const Item: TListBoxItem);
    procedure edtScanChange(Sender: TObject);
    procedure btnAgregarClick(Sender: TObject);
    procedure edtScanClick(Sender: TObject);
    procedure lbDetalleItemClick(const Sender: TCustomListBox;
      const Item: TListBoxItem);
    procedure LimpiarDetalle;
    procedure LimpiarDetalle2;
    procedure btnConfirmarClick(Sender: TObject);
    procedure cbbProveedoresClosePopup(Sender: TObject);
    procedure dtpDesdeClosePicker(Sender: TObject);
    procedure dtpHastaClosePicker(Sender: TObject);
    procedure fdCabeceraCalcFields(DataSet: TDataSet);
    function PermitirRecepcion():Boolean;
    function PrimerDiaMes(dFecha: TDateTime):TDateTime;
    procedure edtCantidadKeyDown(Sender: TObject; var Key: Word;
      var KeyChar: Char; Shift: TShiftState);
    procedure edtCantidadClick(Sender: TObject);
    Procedure MostrarTeclado();
    procedure OcultarTeclado();
    procedure calcularTotales();
    procedure calcularTotales2();
    procedure cbbFiltro1ClosePopup(Sender: TObject);
    procedure edtScan2Change(Sender: TObject);
    procedure cbbFiltro2ClosePopup(Sender: TObject);
    procedure edtScan2Click(Sender: TObject);
    procedure btnAgregar2Click(Sender: TObject);
    procedure edtNumeroRecepcionClick(Sender: TObject);
    procedure lbRecepcionDetalleItemClick(const Sender: TCustomListBox;
      const Item: TListBoxItem);
    procedure edtCantidad2Click(Sender: TObject);
    procedure edtCantidad2KeyDown(Sender: TObject; var Key: Word;
      var KeyChar: Char; Shift: TShiftState);
    procedure btnTecladoClick(Sender: TObject);
    Procedure MostrarOcultarTeclado();
    procedure fdRecepcionDetalleAfterScroll(DataSet: TDataSet);
    procedure btnConfirmar2Click(Sender: TObject);
    procedure btnEliminarClick(Sender: TObject);
  private
   fProvider: TPubSubProvider;
    procedure Split(const aString, Delimiter: string; Results: TStrings);
  public
   property Provider: TPubSubProvider read fProvider write fProvider;
   procedure RecibirCabecera(val: TStringStream);
   procedure RecibirProveedores(val: TStringStream);
   procedure RecibirDetalle(val: TStringStream);
   procedure FiltrarCabecera();
   procedure RecargarDetalle();
   procedure RecargarDetalle2();
   procedure MostrarProgreso;
   procedure TerminarProgreso;
   procedure ConfirmarProcesoCompras(val: string);
   procedure ConfirmarProcesoRecepcion(val: string);
   procedure RecibirRecepcionFiltro1(val: TStringStream);
   procedure RecibirRecepcionFiltro2(val: TStringStream);
   procedure RecibirRecepcionFiltro3(val: TStringStream);
   procedure ShowModalParam(iModo:integer);
  end;

var
  FmRecepcionMercaderia: TFmRecepcionMercaderia;
implementation
 uses
  FMX.Platform, FMX.VirtualKeyBoard, PubSub.Notifications,
  PubSub.InterfaceActions,
  {$IFDEF Android}
  Androidapi.JNI.Toast,
  Androidapi.JNI.GraphicsContentViewText,
  {$ENDIF}
  uPrincipal
  ;
{$R *.fmx}
{$R *.GGlass.fmx ANDROID}
{$R *.Macintosh.fmx MACOS}

procedure TFmRecepcionMercaderia.btnAgregar2Click(Sender: TObject);
var
 iCantRec: Real;
begin
  fdRecepcionDetalle.Filtered:=False;
  if not fdRecepcionFiltro3.IsEmpty then
   try
    if (edtNumeroRecepcion.Text)='' then raise Exception.Create('Debe ingresar numero de recepcion.');
    iCantRec:=StrToFloatDef(edtCantidad2.Text,0);
    if iCantRec<=0 then raise Exception.Create('La cantidad debe ser mayor a cero.');
    if btnEliminar.Visible then
     begin
      fdRecepcionDetalle.Filter:=' CodBarra='+edtScan2.Text;
      fdRecepcionDetalle.Filtered:=True;
      fdRecepcionDetalle.Edit;
      fdRecepcionDetalleCodBarra.AsString:=fdRecepcionFiltro3.FieldByName('CodBarra').AsString;
      fdRecepcionDetalleNumRecepcion.AsString:=edtNumeroRecepcion.Text;
      fdRecepcionDetalleId.AsString:=fdRecepcionFiltro3.FieldByName('Id').AsString;
      fdRecepcionDetalleDescripcion.AsString:=fdRecepcionFiltro3.FieldByName('Descripcion').AsString;
      fdRecepcionDetalleCantidad.AsFloat:=iCantRec;
      fdRecepcionDetalle.Filtered:=False;
     end
    else
     begin
      fdRecepcionDetalle.Open;
      fdRecepcionDetalle.Append;
      fdRecepcionDetalleCodBarra.AsString:=fdRecepcionFiltro3.FieldByName('CodBarra').AsString;
      fdRecepcionDetalleNumRecepcion.AsString:=edtNumeroRecepcion.Text;
      fdRecepcionDetalleId.AsString:=fdRecepcionFiltro3.FieldByName('Id').AsString;
      fdRecepcionDetalleDescripcion.AsString:=fdRecepcionFiltro3.FieldByName('Descripcion').AsString;
      fdRecepcionDetalleCantidad.AsFloat:=iCantRec;
      fdRecepcionDetalle.Post;
     end;
    RecargarDetalle2;
    LimpiarDetalle2;
    calcularTotales2;
   except
    on e:exception do
     begin
       fdRecepcionDetalle.Filtered:=False;
        {$IFDEF Android}
           Toast(e.Message, LongToast);
        {$ENDIF}
     end;
   end;
end;

procedure TFmRecepcionMercaderia.ShowModalParam(iModo:integer);
begin

 if iModo=1 then //Desde una Orden de Compra
  begin
   tbcRecepcion.ActiveTab := TCabecera;
   dtpDesde.DateTime:=PrimerDiaMes(Now);
   MostrarProgreso;
   FmPrincipal.ObtenerProveedores;
   MostrarProgreso;
   FmPrincipal.ObtenerCabeceras;
  end
 else //Sin Orden de Compra
  begin
   tbcRecepcion.ActiveTab := TSinOC;
   MostrarProgreso;
   FmPrincipal.ObtenerRecepcionFiltro1;
   btnTeclado.Visible:=True;
   btnTeclado.Enabled:=True;
   btnAgregar2.Visible:=False;
   btnEliminar.Visible:=False;
  end;

  OcultarTeclado();
  Show;
end;

procedure TFmRecepcionMercaderia.btnAgregarClick(Sender: TObject);
var
 iCantRec: Real;
begin
  if not fdDetalle.IsEmpty then
   try
    iCantRec:=StrToFloatDef(edtCantidad.Text,0);
    if iCantRec>(fdDetalleiCantidad.AsFloat) then raise Exception.Create('No se puede recepcionar cantidad supuerior a la Orden.');
    fdDetalle.Edit;
    fdDetalleiCantidadRecepcionada.AsFloat:=iCantRec;
    RecargarDetalle;
    LimpiarDetalle;
    calcularTotales;
   except
    on e:exception do
     begin
        {$IFDEF Android}
           Toast(e.Message, LongToast);
        {$ENDIF}
     end;
   end;
end;


procedure TFmRecepcionMercaderia.calcularTotales();
var
 rCantidad: Real;
begin
 try
 fdDetalle.Filtered:=False;
  rCantidad:=0;
  if not fdDetalle.IsEmpty then
   begin
    fdDetalle.First;
    while not fdDetalle.Eof do
     begin
       rCantidad:=rCantidad+fdDetalleiCantidadRecepcionada.AsFloat;
      fdDetalle.Next;
     end;
   end;

  if rCantidad>0 then
   begin
    btnConfirmar.Enabled:=True;
    btnConfirmar.TintColor:=$FF5FA07C;
    btnConfirmar.Text:='Confirmar '+FormatFloat('0.00',rCantidad);
   end
  else
   begin
    btnConfirmar.Enabled:=False;
    btnConfirmar.TintColor:=$FFE65934;
    btnConfirmar.Text:='Confirmar ';
   end;

 except
  on e:Exception do
   begin

   end;
 end;
end;

procedure TFmRecepcionMercaderia.calcularTotales2();
var
 rCantidad: Real;
begin
 try
 fdRecepcionDetalle.Filtered:=False;
  rCantidad:=0;
  if not fdRecepcionDetalle.IsEmpty then
   begin
    fdRecepcionDetalle.First;
    while not fdRecepcionDetalle.Eof do
     begin
       rCantidad:=rCantidad+fdRecepcionDetalle.FieldByName('Cantidad').AsFloat;
      fdRecepcionDetalle.Next;
     end;
   end;

  if rCantidad>0 then
   begin
    btnConfirmar2.Enabled:=True;
    btnConfirmar2.TintColor:=$FF5FA07C;
    btnConfirmar2.Text:='Confirmar '+FormatFloat('0.00',rCantidad);
   end
  else
   begin
    btnConfirmar2.Enabled:=False;
    btnConfirmar2.TintColor:=$FFE65934;
    btnConfirmar2.Text:='Confirmar ';
   end;

 except
  on e:Exception do
   begin

   end;
 end;
end;


procedure TFmRecepcionMercaderia.ConfirmarProcesoCompras(val: string);
begin
  if val = 'OK' then
   begin
    btnAtrasClick(nil);
    {$IFDEF Android}
      Toast('Se recepciono correctamente.', LongToast);
    {$ENDIF}
    MostrarProgreso;
    FmPrincipal.ObtenerCabeceras;
   end
  else
   ShowMessage('ocurrio un error.'#10#13+val);
   TerminarProgreso;
end;

procedure TFmRecepcionMercaderia.ConfirmarProcesoRecepcion(val: string);
begin
  if val = 'OK' then
   begin
    {$IFDEF Android}
      Toast('Se recepciono correctamente.', LongToast);
    {$ENDIF}
    MostrarProgreso;
    LimpiarDetalle2;
    edtNumeroRecepcion.Text:='';
    fdRecepcionDetalle.EmptyDataSet;
    calcularTotales2;
    RecargarDetalle2;
    edtNumeroRecepcion.SetFocus;
   end
  else
   ShowMessage('ocurrio un error.'#10#13+val);
   TerminarProgreso;
end;


procedure TFmRecepcionMercaderia.LimpiarDetalle;
begin
  try
   edtScan.Text := '';
   lblProducto.Text := '';
   edtCantidad.Text:='';
   edtScan.SetFocus;
   OcultarTeclado;
   btnAgregar.Visible:=False;
  except
   on e:exception do
    begin
     ShowMessage(e.Message);
    end;
  end;
end;

procedure TFmRecepcionMercaderia.LimpiarDetalle2;
begin
  try
   edtScan2.Text := '';
   lblProducto2.Text := '';
   edtCantidad2.Text:='';
   edtScan2.SetFocus;
   if edtScan.TextPrompt='escanee el codigo' then
    OcultarTeclado
   else
    MostrarTeclado;
   btnAgregar2.Visible:=False;
   btnEliminar.Visible:=False;
  except
   on e:exception do
    begin
     ShowMessage(e.Message);
    end;
  end;
end;


function TFmRecepcionMercaderia.PermitirRecepcion():Boolean;
var
 bResult: Boolean;
begin
  try
   bResult:=True;
   if fdDetalle.IsEmpty then
     bResult:=False
   else
    begin
     bResult:=False;
     fdDetalle.First;
     while not fdDetalle.Eof do
      begin
        if fdDetalle.FieldByName('iCantidadRecepcionada').AsFloat>0 then
         begin
           bResult:=True;
           Break;
         end;
         fdDetalle.Next
      end;
    end;


    if not bResult then
     begin
      {$IFDEF Android}
         Toast('No hay mercaderia por recepcionar.', LongToast);
      {$ENDIF}
     end;
     Result:=bResult;
  finally

  end;
end;

procedure TFmRecepcionMercaderia.btnConfirmar2Click(Sender: TObject);
var
 oStr: TStringStream;
begin
  if not fdRecepcionDetalle.IsEmpty then
   try
    MostrarProgreso;
    oStr:=TStringStream.Create;
    fdRecepcionDetalle.SaveToStream(oStr,sfXML);
    FmPrincipal.InformarRecepcionMercaderiaSinOC(oStr);
   except
    on e: Exception do
     begin
      ShowMessage(e.Message);
      TerminarProgreso;
     end;
   end;
end;

procedure TFmRecepcionMercaderia.btnConfirmarClick(Sender: TObject);
var
 oStr: TStringStream;
begin
  //if PermitirRecepcion then
   try
    MostrarProgreso;
    oStr:=TStringStream.Create;
    fdDetalle.SaveToStream(oStr,sfXML);
    FmPrincipal.InformarRecepcionMercaderia(oStr);
   except
    on e: Exception do
     begin
      ShowMessage(e.Message);
      TerminarProgreso;
     end;
   end;
end;

procedure TFmRecepcionMercaderia.btnEliminarClick(Sender: TObject);
begin
  fdRecepcionDetalle.Filtered:=False;
  if not fdRecepcionFiltro3.IsEmpty then
   try
      fdRecepcionDetalle.Filter:=' CodBarra='+edtScan2.Text;
      fdRecepcionDetalle.Filtered:=True;
      fdRecepcionDetalle.Delete;
      fdRecepcionDetalle.Filtered:=False;
    RecargarDetalle2;
    LimpiarDetalle2;
    calcularTotales2;
   except
    on e:exception do
     begin
       fdRecepcionDetalle.Filtered:=False;
        {$IFDEF Android}
           Toast(e.Message, LongToast);
        {$ENDIF}
     end;
   end;
end;

procedure TFmRecepcionMercaderia.btnTecladoClick(Sender: TObject);
begin
MostrarOcultarTeclado();
end;

Procedure TFmRecepcionMercaderia.MostrarOcultarTeclado();
var
  KeyboardService: IFMXVirtualKeyboardService;
  bCambio: Boolean;
begin
 bCambio:=False;
 if TPlatformServices.Current.SupportsPlatformService(IFMXVirtualKeyboardService, IInterface(KeyboardService)) then
  begin
   //if not (TVirtualKeyboardState.Visible in KeyboardService.VirtualKeyboardState) then
   if edtScan2.TextPrompt='escanee el codigo' then
    begin
     KeyboardService.ShowVirtualKeyboard(edtScan);
     bCambio:=True;
     edtScan2.TextPrompt:='ingrese el codigo';
     FMX.Types.VKAutoShowMode :=TVKAutoShowMode.Always;
    end;
   //if (TVirtualKeyboardState.Visible in KeyboardService.VirtualKeyboardState) and (not bCambio) then
   if (edtScan2.TextPrompt='ingrese el codigo') and (not bCambio) then
    begin
     KeyboardService.HideVirtualKeyboard;
     edtScan2.TextPrompt:='escanee el codigo';
     FMX.Types.VKAutoShowMode :=TVKAutoShowMode.Never;
    end;
  end;
end;

procedure TFmRecepcionMercaderia.cbbFiltro1ClosePopup(Sender: TObject);
var
 sFiltro1:string;
begin
 sFiltro1:='-1';
 if fdRecepcionFiltro1.RecordCount>0 then sFiltro1:=cbbFiltro1.ListItems[cbbFiltro1.ItemIndex].TagString;
  MostrarProgreso;
  FmPrincipal.ObtenerRecepcionFiltro2(sFiltro1);
end;

procedure TFmRecepcionMercaderia.cbbFiltro2ClosePopup(Sender: TObject);
var
 sFiltro1, sFiltro2: string;
begin
 sFiltro1:='-1';
 sFiltro2:='-1';
 if fdRecepcionFiltro1.RecordCount>0 then sFiltro1:=cbbFiltro1.ListItems[cbbFiltro1.ItemIndex].TagString;
 if fdRecepcionFiltro2.RecordCount>0 then sFiltro2:=cbbFiltro2.ListItems[cbbFiltro2.ItemIndex].TagString;
 MostrarProgreso;
 FmPrincipal.ObtenerRecepcionFiltro3(sFiltro1,sFiltro2);
end;

procedure TFmRecepcionMercaderia.cbbProveedoresClosePopup(Sender: TObject);
begin
 FiltrarCabecera();
end;

procedure TFmRecepcionMercaderia.dtpDesdeClosePicker(Sender: TObject);
begin
 FiltrarCabecera();
end;

procedure TFmRecepcionMercaderia.dtpHastaClosePicker(Sender: TObject);
begin
 FiltrarCabecera();
end;

procedure TFmRecepcionMercaderia.edtCantidad2Click(Sender: TObject);
begin
MostrarTeclado();
end;

procedure TFmRecepcionMercaderia.edtCantidad2KeyDown(Sender: TObject;
  var Key: Word; var KeyChar: Char; Shift: TShiftState);
begin
  If Key = vkReturn Then
  begin
//    Key := #0;
    btnAgregar2Click(nil);
  end
end;

procedure TFmRecepcionMercaderia.edtCantidadClick(Sender: TObject);
begin
  MostrarTeclado();
end;

procedure TFmRecepcionMercaderia.edtCantidadKeyDown(Sender: TObject;
  var Key: Word; var KeyChar: Char; Shift: TShiftState);
begin
  If Key = vkReturn Then
  begin
//    Key := #0;
    btnAgregarClick(nil);
  end
end;

procedure TFmRecepcionMercaderia.edtNumeroRecepcionClick(Sender: TObject);
begin
MostrarTeclado();
end;

procedure TFmRecepcionMercaderia.edtScan2Change(Sender: TObject);
var
 sDescripcion:string;
begin
  try
    btnAgregar2.Visible:=False;
    sDescripcion:='';
    if edtScan2.Text<>'' then sDescripcion:='Producto no encontrado';

   fdRecepcionFiltro3.Filtered:=False;
    fdRecepcionFiltro3.Filter:=' CodBarra='+QuotedStr(edtScan2.Text);
    fdRecepcionFiltro3.Filtered:=True;
    if (not fdRecepcionFiltro3.IsEmpty) and (edtScan2.Text<>'') then
     begin
      sDescripcion:=fdRecepcionFiltro3.FieldByName('Descripcion').AsString;
      edtCantidad2.Text:='0.00';
      edtCantidad2.SelectAll;
      edtCantidad2.SetFocus;
      MostrarTeclado();
      btnAgregar2.Visible:=True;
      btnEliminar.Visible:=True;
     end;
    lblProducto2.Text:=sDescripcion;
  except
   on e:exception do
    begin
      ShowMessage(e.Message);
    end;
  end;
end;

procedure TFmRecepcionMercaderia.edtScan2Click(Sender: TObject);
begin
 LimpiarDetalle2;
end;

procedure TFmRecepcionMercaderia.edtScanChange(Sender: TObject);
var
 sDescripcion:string;
begin
  try
    btnAgregar.Visible:=False;
    sDescripcion:='';
    if edtScan.Text<>'' then sDescripcion:='Producto no encontrado';

    fdDetalle.Filtered:=False;
    fdDetalle.Filter:=' sCodBarra='+QuotedStr(edtScan.Text);
    fdDetalle.Filtered:=True;
    if (not fdDetalle.IsEmpty) and (edtScan.Text<>'') then
     begin
      sDescripcion:=fdDetallesNombre.AsString;
      edtCantidad.Text:=FormatFloat('0.00',fdDetalleiCantidad.AsFloat);//-fdDetalleiCantidadRecepcionada.AsFloat);
      edtCantidad.SelectAll;
      edtCantidad.SetFocus;
      MostrarTeclado();
      btnAgregar.Visible:=True;
     end;
    lblProducto.Text:=sDescripcion;
  except
   on e:exception do
    begin
      ShowMessage(e.Message);
    end;
  end;
end;

Procedure TFmRecepcionMercaderia.MostrarTeclado();
var
  KeyboardService: IFMXVirtualKeyboardService;
begin
 if TPlatformServices.Current.SupportsPlatformService(IFMXVirtualKeyboardService, IInterface(KeyboardService)) then
  begin
   if not (TVirtualKeyboardState.Visible in KeyboardService.VirtualKeyboardState) then
    KeyboardService.ShowVirtualKeyboard(self);
  end;
end;

procedure TFmRecepcionMercaderia.OcultarTeclado();
var
  KeyboardService: IFMXVirtualKeyboardService;
begin
 if TPlatformServices.Current.SupportsPlatformService(IFMXVirtualKeyboardService, IInterface(KeyboardService)) then
  begin
   if (TVirtualKeyboardState.Visible in KeyboardService.VirtualKeyboardState) then
    KeyboardService.HideVirtualKeyboard;
  end;
  FMX.Types.VKAutoShowMode :=TVKAutoShowMode.vkasNever;
end;

procedure TFmRecepcionMercaderia.edtScanClick(Sender: TObject);
begin
 OcultarTeclado;
 LimpiarDetalle;
end;

procedure TFmRecepcionMercaderia.fdCabeceraCalcFields(DataSet: TDataSet);
begin
 fdCabecerafechafloat.AsFloat:=fdCabecerafDocumento.AsFloat;
end;

procedure TFmRecepcionMercaderia.fdRecepcionDetalleAfterScroll(
  DataSet: TDataSet);
begin
 edtNumeroRecepcion.ReadOnly := not fdRecepcionDetalle.IsEmpty;
end;

procedure TFmRecepcionMercaderia.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    OcultarTeclado;
    Action := TCloseAction.caFree;
end;

procedure TFmRecepcionMercaderia.FormCreate(Sender: TObject);
begin
  fProvider := TPubSubProvider.Create;
end;

function TFmRecepcionMercaderia.PrimerDiaMes(dFecha: TDateTime):TDateTime;
var
 dia, mes, anio : Word;
begin
  DecodeDate(dFecha,anio,mes,dia);
	Result := EncodeDate(anio,mes,1);
end;

procedure TFmRecepcionMercaderia.lbCabeceraItemClick(
  const Sender: TCustomListBox; const Item: TListBoxItem);
var
 sNumDoc, id, sProveedor, tipo,suc,numdoc: string;
 res: TStringList;
begin
 fdDetalle.Filtered:=False;
 lbdetalle.Items.Clear;
 if lbCabecera.Items.Count>0 then
  begin
    sNumDoc:=lbCabecera.ItemByIndex(lbCabecera.ItemIndex).StylesData['snumdoc'].ToString;
    id:=lbCabecera.ItemByIndex(lbCabecera.ItemIndex).StylesData['idprov'].ToString;
    sProveedor:=lbCabecera.ItemByIndex(lbCabecera.ItemIndex).StylesData['proveedor'].ToString;
    res := TStringList.Create();
    split(sNumDoc,'-',res);
    tipo:=res[0];
    suc:=res[1];
    numdoc:=res[2];
   if not fdCabecera.IsEmpty then
    begin
      tbcRecepcion.ActiveTab := TDetalle;
      lblNumero.Text:=sNumDoc;
      lblProveedor.Text:=sProveedor;
      FmPrincipal.ObtenerDetalle(tipo,
                                 StrToInt(id),
                                 StrToInt(suc),
                                 StrToInt(numdoc));
    end;
  end;
end;

procedure TFmRecepcionMercaderia.lbDetalleItemClick(
  const Sender: TCustomListBox; const Item: TListBoxItem);
var
 res: TStringList;
begin
 LimpiarDetalle;
 if lbdetalle.Items.Count>0 then
  begin
   edtScan.Text:=lbdetalle.ItemByIndex(lbdetalle.ItemIndex).StylesData['codigo'].ToString;
  end;
end;

procedure TFmRecepcionMercaderia.lbRecepcionDetalleItemClick(
  const Sender: TCustomListBox; const Item: TListBoxItem);
var
 res: TStringList;
begin
 LimpiarDetalle2;
 if lbRecepcionDetalle.Items.Count>0 then
  begin
   btnEliminar.Visible:=True;
   edtScan2.Text:=lbRecepcionDetalle.ItemByIndex(lbRecepcionDetalle.ItemIndex).StylesData['codigo'].ToString;
   lblProducto2.Text:=lbRecepcionDetalle.ItemByIndex(lbRecepcionDetalle.ItemIndex).StylesData['text'].ToString;
   edtCantidad2.Text:=lbRecepcionDetalle.ItemByIndex(lbRecepcionDetalle.ItemIndex).StylesData['cantidadrec'].ToString;
   btnAgregar2.Visible:=True;
  end;
end;


procedure TFmRecepcionMercaderia.btnAtrasClick(Sender: TObject);
begin
 if tbcRecepcion.ActiveTab=TDetalle then
  tbcRecepcion.ActiveTab:=TCabecera
 else
  Close;
end;

procedure TFmRecepcionMercaderia.Split(const aString, Delimiter: string; Results: TStrings);
var TempString:string;
begin
  TempString:= StringReplace(aString,'"',' ',[rfReplaceAll]);
  Results.CommaText := '"' + StringReplace (TempString, Delimiter, '","',
    [rfReplaceAll]) + '"';
  if Results.Count >= 1 then
   begin
    //elimino la linea vacia al final si existe
    if Results[Results.Count - 1] = '' then
    begin
      Results.Delete (Results.Count - 1);
    end;
   end;
end;

procedure TFmRecepcionMercaderia.RecibirProveedores(val: TStringStream);
var
 Item: TListBoxItem;
begin
 try
  fdProveedores.LoadFromStream(val,sfXML);
  cbbProveedores.Items.Clear;
  cbbProveedores.ListBox.BeginUpdate;
   if not fdProveedores.IsEmpty then
    begin
      fdProveedores.First;
      while not fdProveedores.Eof do
       begin
          begin
            Item := TListBoxItem.Create(nil);
            Item.Text := fdProveedores.FieldByName('sRazonSocial').AsString;
            Item.Parent := cbbProveedores.ListBox;
            Item.StyledSettings := Item.StyledSettings - [TStyledSetting.FontColor, TStyledSetting.Size];
            Item.Tag := (fdProveedores.FieldByName('idProveedor').AsInteger);
          end;
         fdProveedores.Next;
       end;
    end;
   cbbProveedores.ListBox.EndUpdate;
   cbbProveedores.ItemIndex:=0;
 except
  on e:Exception do
   begin
    ShowMessage(e.Message);
   end;
 end;
end;



procedure TFmRecepcionMercaderia.RecibirRecepcionFiltro1(val: TStringStream);
var
 Item: TListBoxItem;
 sFiltro1:string;
begin
 try
  MostrarProgreso;
  fdRecepcionFiltro1.LoadFromStream(val,sfXML);
  cbbFiltro1.Items.Clear;
  cbbFiltro1.ListBox.BeginUpdate;
   if not fdRecepcionFiltro1.IsEmpty then
    begin
      cbbFiltro1.Enabled:=True;
      fdRecepcionFiltro1.First;
      while not fdRecepcionFiltro1.Eof do
       begin
          begin
            Item := TListBoxItem.Create(nil);
            Item.Text := fdRecepcionFiltro1.FieldByName('Descripcion').AsString;
            Item.Parent := cbbFiltro1.ListBox;
            Item.StyledSettings := Item.StyledSettings - [TStyledSetting.FontColor, TStyledSetting.Size];
            Item.TagString := (fdRecepcionFiltro1.FieldByName('Id').AsString);
          end;
         fdRecepcionFiltro1.Next;
       end;
    end;
   cbbFiltro1.ListBox.EndUpdate;
   cbbFiltro1.ItemIndex:=0;
   sFiltro1:='-1';
   if fdRecepcionFiltro1.RecordCount>0 then sFiltro1:=cbbFiltro1.ListItems[cbbFiltro1.ItemIndex].TagString;
    FmPrincipal.ObtenerRecepcionFiltro2(sFiltro1);
   TerminarProgreso;
 except
  on e:Exception do
   begin
    ShowMessage(e.Message);
   end;
 end;
end;

procedure TFmRecepcionMercaderia.RecibirRecepcionFiltro2(val: TStringStream);
var
 Item: TListBoxItem;
 sFiltro1,sFiltro2: string;
begin
 try
  MostrarProgreso;
  fdRecepcionFiltro2.LoadFromStream(val,sfXML);
  cbbFiltro2.Items.Clear;
  cbbFiltro2.ListBox.BeginUpdate;
  Item := TListBoxItem.Create(nil);
  Item.Text := '<Sin asignar>';
  Item.Parent := cbbFiltro2.ListBox;
  Item.StyledSettings := Item.StyledSettings - [TStyledSetting.FontColor, TStyledSetting.Size];
  Item.TagString := '-1';
   if not fdRecepcionFiltro2.IsEmpty then
    begin
      cbbFiltro2.Enabled:=True;
      fdRecepcionFiltro2.First;
      while not fdRecepcionFiltro2.Eof do
       begin
          begin
            Item := TListBoxItem.Create(nil);
            Item.Text := fdRecepcionFiltro2.FieldByName('Descripcion').AsString;
            Item.Parent := cbbFiltro2.ListBox;
            Item.StyledSettings := Item.StyledSettings - [TStyledSetting.FontColor, TStyledSetting.Size];
            Item.TagString := (fdRecepcionFiltro2.FieldByName('Id').AsString);
          end;
         fdRecepcionFiltro2.Next;
       end;
    end;
   cbbFiltro2.ListBox.EndUpdate;
   cbbFiltro2.ItemIndex:=0;

   sFiltro1:='-1';
   sFiltro2:='-1';
   if fdRecepcionFiltro1.RecordCount>0 then sFiltro1:=cbbFiltro1.ListItems[cbbFiltro1.ItemIndex].TagString;
   if fdRecepcionFiltro2.RecordCount>0 then sFiltro2:=cbbFiltro2.ListItems[cbbFiltro2.ItemIndex].TagString;
   MostrarProgreso;
   FmPrincipal.ObtenerRecepcionFiltro3(sFiltro1,sFiltro2);
   TerminarProgreso;
 except
  on e:Exception do
   begin
    ShowMessage(e.Message);
   end;
 end;
end;


procedure TFmRecepcionMercaderia.RecibirRecepcionFiltro3(val: TStringStream);
var
 Item: TListBoxItem;
begin
 try
  MostrarProgreso;
  fdRecepcionFiltro3.LoadFromStream(val,sfXML);
  TerminarProgreso;
 except
  on e:Exception do
   begin
    ShowMessage(e.Message);
   end;
 end;
end;

procedure TFmRecepcionMercaderia.RecibirCabecera(val: TStringStream);
begin
 try
  try
    fdCabecera.LoadFromStream(val,sfXML);
  finally
    FiltrarCabecera();
    TerminarProgreso;
  end;
 except
  on e:Exception do
   begin
    ShowMessage(e.Message);
   end;
 end;
end;

procedure TFmRecepcionMercaderia.FiltrarCabecera();
var
 sFiltro: string;
 fdesde,fhasta: TDateTime;
begin
 MostrarProgreso;
 if fdCabecera.Active then
 try
  try
    fdesde:=Trunc(dtpDesde.DateTime);
    fhasta:=Trunc(dtpHasta.DateTime);
    sFiltro:='';
    fdCabecera.Filtered:=False;
    if Integer(cbbProveedores.ListItems[cbbProveedores.ItemIndex].Tag)>-1 then
     begin
      if sFiltro<>'' then sFiltro:=sFiltro+' AND ';
      sFiltro:=sFiltro+' idProveedor='+IntToStr(Integer(cbbProveedores.ListItems[cbbProveedores.ItemIndex].Tag));//Integer(cbbProveedores.Items.Objects[cbbProveedores.ItemIndex]));
     end;

      if sFiltro<>'' then sFiltro:=sFiltro+' AND ';
      sFiltro:=sFiltro+' fechafloat >= '+FloatToStr(fdesde)+' AND fechafloat <= '+FloatToStr(fhasta);

    fdCabecera.Filter:=sFiltro;
    fdCabecera.Filtered:=(sFiltro<>'');

    lbCabecera.Items.Clear;
    lbCabecera.BeginUpdate;
  finally
     if not fdCabecera.IsEmpty then
      begin
        fdCabecera.First;
        while not fdCabecera.Eof do
         begin
            lbCabecera.Items.AddObject(fdCabecera.FieldByName('sNumDoc').AsString, nil);
            lbCabecera.ItemByIndex(lbCabecera.Count-1).StyleLookup := 'OC';
            lbCabecera.ItemByIndex(lbCabecera.Count-1).height := 70;
            lbCabecera.ItemByIndex(lbCabecera.Count-1).StylesData['snumdoc'] := fdCabecera.FieldByName('sNumDoc').AsString;
            lbCabecera.ItemByIndex(lbCabecera.Count-1).StylesData['proveedor'] := fdCabecera.FieldByName('sRazonSocial').AsString;
            lbCabecera.ItemByIndex(lbCabecera.Count-1).StylesData['fecha'] := fdCabecera.FieldByName('fDocumento').AsString;
            lbCabecera.ItemByIndex(lbCabecera.Count-1).StylesData['idprov'] := fdCabecera.FieldByName('idProveedor').AsString;
           fdCabecera.Next;
         end;
      end;
    lbCabecera.EndUpdate;
  end;
 except
  on e:Exception do
   begin
    ShowMessage(e.Message);
   end;
 end;
 TerminarProgreso;
end;


procedure TFmRecepcionMercaderia.RecargarDetalle();
begin
  try
   try
    btnAgregar.Visible:=False;
    lbdetalle.Items.Clear;
    lbdetalle.BeginUpdate;
     fdDetalle.Filtered:=False;
     if not fdDetalle.IsEmpty then
      begin
        fdDetalle.First;
        while not fdDetalle.Eof do
         begin
            lbdetalle.Items.AddObject(fdDetalle.FieldByName('sCodBarra').AsString, nil);
            lbdetalle.ItemByIndex(lbdetalle.Count-1).StyleLookup := 'ItemOC';
            lbdetalle.ItemByIndex(lbdetalle.Count-1).height := 70;
            lbdetalle.ItemByIndex(lbdetalle.Count-1).StylesData['codigo'] := fdDetalle.FieldByName('sCodBarra').AsString;
            lbdetalle.ItemByIndex(lbdetalle.Count-1).StylesData['text'] := fdDetalle.FieldByName('sNombre').AsString;
            lbdetalle.ItemByIndex(lbdetalle.Count-1).StylesData['cantidad'] := FormatFloat('0.00',fdDetalle.FieldByName('iCantidad').AsFloat);
            lbdetalle.ItemByIndex(lbdetalle.Count-1).StylesData['cantidadrec'] := FormatFloat('0.00',fdDetalle.FieldByName('iCantidadRecepcionada').AsFloat);
           fdDetalle.Next;
         end;
      end;
   except
    on e: Exception do
     begin
      ShowMessage(e.Message);
     end;
   end;
  finally
    lbdetalle.EndUpdate;
    lbdetalle.Repaint;
  end;
end;

procedure TFmRecepcionMercaderia.RecargarDetalle2();
begin
  try
   try
    btnAgregar2.Visible:=False;
    lbRecepcionDetalle.Items.Clear;
    lbRecepcionDetalle.BeginUpdate;
     fdRecepcionDetalle.Filtered:=False;
     if not fdRecepcionDetalle.IsEmpty then
      begin
        fdRecepcionDetalle.First;
        while not fdRecepcionDetalle.Eof do
         begin
            lbRecepcionDetalle.Items.AddObject(fdRecepcionDetalle.FieldByName('CodBarra').AsString, nil);
            lbRecepcionDetalle.ItemByIndex(lbRecepcionDetalle.Count-1).StyleLookup := 'ItemOC';
            lbRecepcionDetalle.ItemByIndex(lbRecepcionDetalle.Count-1).height := 70;
            lbRecepcionDetalle.ItemByIndex(lbRecepcionDetalle.Count-1).StylesData['codigo'] := fdRecepcionDetalle.FieldByName('CodBarra').AsString;
            lbRecepcionDetalle.ItemByIndex(lbRecepcionDetalle.Count-1).StylesData['text'] := fdRecepcionDetalle.FieldByName('Descripcion').AsString;
            lbRecepcionDetalle.ItemByIndex(lbRecepcionDetalle.Count-1).StylesData['cantidad'] := '';
            lbRecepcionDetalle.ItemByIndex(lbRecepcionDetalle.Count-1).StylesData['cantidadrec'] := FormatFloat('0.00',fdRecepcionDetalle.FieldByName('Cantidad').AsFloat);
           fdRecepcionDetalle.Next;
         end;
      end;
   except
    on e: Exception do
     begin
      ShowMessage(e.Message);
     end;
   end;
  finally
    lbRecepcionDetalle.EndUpdate;
    lbRecepcionDetalle.Repaint;
  end;
end;

procedure TFmRecepcionMercaderia.RecibirDetalle(val: TStringStream);
begin
  try
   try
    btnAgregar.Visible:=False;
    fdDetalle.LoadFromStream(val,sfXML);
    lbdetalle.Items.Clear;
    lbdetalle.BeginUpdate;
     if not fdDetalle.IsEmpty then
      begin
        fdDetalle.First;
        while not fdDetalle.Eof do
         begin
            lbdetalle.Items.AddObject(fdDetalle.FieldByName('sCodBarra').AsString, nil);
            lbdetalle.ItemByIndex(lbdetalle.Count-1).StyleLookup := 'ItemOC';
            lbdetalle.ItemByIndex(lbdetalle.Count-1).height := 70;
            lbdetalle.ItemByIndex(lbdetalle.Count-1).StylesData['codigo'] := fdDetalle.FieldByName('sCodBarra').AsString;
            lbdetalle.ItemByIndex(lbdetalle.Count-1).StylesData['text'] := fdDetalle.FieldByName('sNombre').AsString;
            lbdetalle.ItemByIndex(lbdetalle.Count-1).StylesData['cantidad'] := FormatFloat('0.00',fdDetalle.FieldByName('iCantidad').AsFloat);
            lbdetalle.ItemByIndex(lbdetalle.Count-1).StylesData['cantidadrec'] := FormatFloat('0.00',0);
           fdDetalle.Next;
         end;
      end;
   except
    on e: Exception do
     begin
      ShowMessage(e.Message);
     end;
   end;
  finally
    lbdetalle.EndUpdate;
    TerminarProgreso;
    calcularTotales;
  end;
end;


procedure TFmRecepcionMercaderia.MostrarProgreso;
begin
 try
  aiProgreso.Visible := True;
  aiProgreso.BringToFront;
  aiProgreso.Enabled := True;
  Application.ProcessMessages;
 except
  on e:Exception do
   begin
    ShowMessage(E.Message);
   end;
 end;
end;

procedure TFmRecepcionMercaderia.TerminarProgreso;
begin
 try
  aiProgreso.SendToBack;
  aiProgreso.Visible := False;
  aiProgreso.Enabled := False;
  Application.ProcessMessages;
 except
  on e:Exception do
   begin
    ShowMessage(E.Message);
   end;
 end;
end;

end.
