unit uScanner;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.StdCtrls,
  FMX.Controls.Presentation, FMX.Edit, FMX.Layouts, FMX.Objects, FMX.Effects,
  PubSub.Provider, Globales;

type
  TFmScanner = class(TForm)
    Header: TToolBar;
    HeaderLabel: TLabel;
    SpeedButton2: TSpeedButton;
    secProducto: TLayout;
    lblProducto: TLabel;
    secScanner: TLayout;
    edtScan: TEdit;
    StyleBook1: TStyleBook;
    pnInformar: TPanel;
    btnInformar: TButton;
    Label1: TLabel;
    Rectangle1: TRectangle;
    ShadowEffect1: TShadowEffect;
    lblCodigo: TLabel;
    Image1: TImage;
    lblPrecio: TLabel;
    tmrBuscaDatosProducto: TTimer;
    aiProgreso: TAniIndicator;
    tmrEsperaConfirmacion: TTimer;
    btnTeclado: TSpeedButton;

    procedure SpeedButton2Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure FormVirtualKeyboardShown(Sender: TObject;
      KeyboardVisible: Boolean; const Bounds: TRect);
    procedure edtScanClick(Sender: TObject);
    procedure edtScanChangeTracking(Sender: TObject);
    procedure btnInformarClick(Sender: TObject);
    procedure Label1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure tmrBuscaDatosProductoTimer(Sender: TObject);
    procedure tmrEsperaConfirmacionTimer(Sender: TObject);
    procedure OcultarTeclado();
    procedure OcultarTeclado2();
    Procedure MostrarTeclado();
    Procedure MostrarOcultarTeclado;
    procedure btnTecladoClick(Sender: TObject);
    procedure secProductoClick(Sender: TObject);
  private
    { Private declarations }
    fProvider: TPubSubProvider;
    objCodigoColectado: TCodigoColectado;
    _codigoInformado: string;
    procedure MostrarProducto(p: TCodigoColectado);
    procedure IngresarOtroCodigo();
    procedure Split(Delimiter: Char; Str: string; ListOfStrings: TStrings);
    procedure MostrarProgreso;
    procedure TerminarProgreso;
    procedure InformarProducto();

  public
    { Public declarations }
    property Provider: TPubSubProvider read fProvider write fProvider;
    procedure RecibirComunicacionProducto(val: String);
    procedure ConfirmarCodigoInformado(cod: string);
  end;

var
  FmScanner: TFmScanner;

implementation

uses
  FMX.Platform, FMX.VirtualKeyBoard, PubSub.Notifications,
  PubSub.InterfaceActions,
  {$IFDEF Android}
  Androidapi.JNI.Toast,
  Androidapi.JNI.GraphicsContentViewText,
  {$ENDIF}
  uPrincipal;

{$R *.fmx}

procedure TFmScanner.Split(Delimiter: Char; Str: string; ListOfStrings: TStrings);
begin
   ListOfStrings.Clear;
   ListOfStrings.Delimiter       := Delimiter;
   ListOfStrings.StrictDelimiter := True; // Requires D2006 or newer.
   ListOfStrings.DelimitedText   := Str;
end;

procedure TFmScanner.btnInformarClick(Sender: TObject);
begin
  _codigoInformado := edtScan.Text;
  MostrarProgreso;
  if FmPrincipal.connected then
  begin
    tmrEsperaConfirmacion.Enabled := true;
    InformarProducto()
  end
  else
  begin
    showmessage('No se encuentra conectado');
    Close;
  end;
end;

procedure TFmScanner.InformarProducto();
var
  tempNotifNuevoCodigo: TNotificationCodigoClass;
begin
  tempNotifNuevoCodigo := TNotificationCodigoClass.Create;
  tempNotifNuevoCodigo.Actions:=[actNuevoCodigo];
  tempNotifNuevoCodigo.ActionValue := objCodigoColectado;
  fProvider.NotifySubscribers(tempNotifNuevoCodigo);
end;

procedure TFmScanner.edtScanChangeTracking(Sender: TObject);
begin
  if length(edtScan.Text) >= 8  then
  begin
    tmrBuscaDatosProducto.Enabled := false;
    tmrBuscaDatosProducto.Enabled := true;
  end;
end;

procedure TFmScanner.IngresarOtroCodigo();
begin
  edtScan.Text := '';
  lblProducto.Text := '';
  pnInformar.Visible := false;
  secProducto.Visible := false;
  edtScan.SetFocus;
  if edtScan.TextPrompt='escanee el codigo' then
   OcultarTeclado
  else
   MostrarTeclado;
end;

procedure TFmScanner.edtScanClick(Sender: TObject);
begin
 IngresarOtroCodigo;
end;

procedure TFmScanner.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  OcultarTeclado;
  Action := TCloseAction.caFree;
end;

procedure TFmScanner.FormCreate(Sender: TObject);
begin
  fProvider := TPubSubProvider.Create;
  objCodigoColectado := TCodigoColectado.Create;
  //OcultarTeclado;
end;

procedure TFmScanner.FormShow(Sender: TObject);
begin
  pnInformar.Visible := false;
  secProducto.Visible := false;
  OcultarTeclado;
  //edtScan.SetFocus;
end;

procedure TFmScanner.FormVirtualKeyboardShown(Sender: TObject;
  KeyboardVisible: Boolean; const Bounds: TRect);
//var
//KeyboardService: IFMXVirtualKeyboardService;
begin
//  if TPlatformServices.Current.SupportsPlatformService(IFMXVirtualKeyboardService,IInterface(KeyboardService)) then
//   if KeyboardService <> nil then
//    KeyboardService.HideVirtualKeyboard;
//  FMX.Types.VKAutoShowMode :=TVKAutoShowMode.vkasNever;
end;

procedure TFmScanner.Label1Click(Sender: TObject);
begin
  IngresarOtroCodigo();
end;

procedure TFmScanner.SpeedButton2Click(Sender: TObject);
begin
  Close;
end;

// Espera por los datos
// Deberia recibir por el procedimiento RecibirComunicacionProducto
procedure TFmScanner.tmrBuscaDatosProductoTimer(Sender: TObject);
begin
  tmrBuscaDatosProducto.Enabled := false;
  if FmPrincipal.connected then
  begin
    tmrEsperaConfirmacion.Enabled := true;
    MostrarProgreso;
    FmPrincipal.BuscraDatos(edtScan.Text)
  end
  else
  begin
    showmessage('No se encuentra conectado');
    Close;
  end;
end;

procedure TFmScanner.tmrEsperaConfirmacionTimer(Sender: TObject);
begin
  tmrEsperaConfirmacion.Enabled := false;
  Showmessage('No se informo el c�digo. Intente de nuevo o revise su conexi�n');
  TerminarProgreso;
end;

procedure TFmScanner.MostrarProducto(p: TCodigoColectado);
begin
  lblProducto.Text := p.nombre;
  lblCodigo.Text := edtScan.Text;
  lblPrecio.text := Format('$ %.2f', [p.precio]);
  secProducto.Visible := true;
  pnInformar.Visible := true;
end;

Procedure TFmScanner.MostrarTeclado();
var
  KeyboardService: IFMXVirtualKeyboardService;
begin
 if TPlatformServices.Current.SupportsPlatformService(IFMXVirtualKeyboardService, IInterface(KeyboardService)) then
  begin
   //if not (TVirtualKeyboardState.Visible in KeyboardService.VirtualKeyboardState) then
    KeyboardService.ShowVirtualKeyboard(edtScan);
  end;
  FMX.Types.VKAutoShowMode :=TVKAutoShowMode.Always;
end;

procedure TFmScanner.OcultarTeclado();
var
  KeyboardService: IFMXVirtualKeyboardService;
begin
 if TPlatformServices.Current.SupportsPlatformService(IFMXVirtualKeyboardService, IInterface(KeyboardService)) then
  begin
   //if (TVirtualKeyboardState.Visible in KeyboardService.VirtualKeyboardState) then
    KeyboardService.HideVirtualKeyboard;
  end;
  FMX.Types.VKAutoShowMode :=TVKAutoShowMode.Never;

end;

procedure TFmScanner.OcultarTeclado2();
var
  KeyboardService: IFMXVirtualKeyboardService;
begin
 if TPlatformServices.Current.SupportsPlatformService(IFMXVirtualKeyboardService, IInterface(KeyboardService)) then
  begin
   if (TVirtualKeyboardState.Visible in KeyboardService.VirtualKeyboardState) then
    KeyboardService.HideVirtualKeyboard;
  end;
end;

Procedure TFmScanner.MostrarOcultarTeclado();
var
  KeyboardService: IFMXVirtualKeyboardService;
  bCambio: Boolean;
begin
 bCambio:=False;
 if TPlatformServices.Current.SupportsPlatformService(IFMXVirtualKeyboardService, IInterface(KeyboardService)) then
  begin
   //if not (TVirtualKeyboardState.Visible in KeyboardService.VirtualKeyboardState) then
   if edtScan.TextPrompt='escanee el codigo' then
    begin
     KeyboardService.ShowVirtualKeyboard(edtScan);
     bCambio:=True;
     edtScan.TextPrompt:='ingrese el codigo';
     FMX.Types.VKAutoShowMode :=TVKAutoShowMode.Always;
    end;
   //if (TVirtualKeyboardState.Visible in KeyboardService.VirtualKeyboardState) and (not bCambio) then
   if (edtScan.TextPrompt='ingrese el codigo') and (not bCambio) then
    begin
     KeyboardService.HideVirtualKeyboard;
     edtScan.TextPrompt:='escanee el codigo';
     FMX.Types.VKAutoShowMode :=TVKAutoShowMode.Never;
    end;
  end;
end;

procedure TFmScanner.RecibirComunicacionProducto(val: String);
var
   res: TStringList;
   precio:string;
begin
 try
  TerminarProgreso;
  tmrEsperaConfirmacion.Enabled := false;
  res := TStringList.Create();
  try
    split('|', val, res);
  finally
    precio:=res[1];
    objCodigoColectado.id := gObtenerGUID32();
    objCodigoColectado.codigo := edtScan.Text;
//    objCodigoColectado.fecha := gDateToStr(now);
    objCodigoColectado.nombre := res[0];
    objCodigoColectado.precio := StrToFloat(res[1]);
    mostrarProducto(objCodigoColectado);
    res.Free;
  end;
 Except
  on e:Exception do
   begin
    Showmessage('No se informo el c�digo. '+e.Message);
   end;
 end;
end;

procedure TFmScanner.secProductoClick(Sender: TObject);
begin
 OcultarTeclado2();
end;

procedure TFmScanner.btnTecladoClick(Sender: TObject);
begin
 MostrarOcultarTeclado();
end;

procedure TFmScanner.ConfirmarCodigoInformado(cod: string);
begin
  if trim(cod) = trim(_codigoInformado) then
  begin
    IngresarOtroCodigo;
    TerminarProgreso;
    tmrEsperaConfirmacion.Enabled := false;
    {$IFDEF Android}
      Toast('El producto ha sido informado correctamente.', LongToast);
    {$ENDIF}
  end;
end;

procedure TFmScanner.MostrarProgreso;
begin
  aiProgreso.Visible := True;
  aiProgreso.BringToFront;
  aiProgreso.Enabled := True;
  Application.ProcessMessages;
end;

procedure TFmScanner.TerminarProgreso;
begin
  aiProgreso.SendToBack;
  aiProgreso.Visible := False;
  aiProgreso.Enabled := False;
  Application.ProcessMessages;
end;

end.
