unit Records.Codigos;

interface

uses
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client, FireDAC.Stan.StorageBin,
  Firedac.stan.StorageJSON;

const
  ARCHIVO_CODIGOS = 'codigos.json';

type
  TCodigosLeidos = class
    private
      _memCodigos : TFDMemTable;
    public
      constructor Create;
      property MemCodigo: TFDMemTable read _memCodigos;
      procedure GuardarCodigo(id: string; cod: string; fecha: string);
  end;

implementation

constructor TCodigosLeidos.Create();
begin
  _memCodigos := TFDMemTable.Create(nil);
  with _memCodigos.FieldDefs do
  begin
   Add('id', ftString, 20, True);
   Add('codigo', ftString, 20, True);
   Add('fecha', ftString, 20, False);
  end;
  _memCodigos.CreateDataset;
//  _memCodigos.LoadFromFile(ARCHIVO_CODIGOS, sfJSON);
end;

procedure TCodigosLeidos.GuardarCodigo(id: string; cod: string; fecha: string);
begin
  with _memCodigos do
  begin
     Append;
     Fields[0].AsString := id;
     Fields[1].AsString := cod;
     Fields[2].AsString := fecha;
     Post;
  end;
  _memCodigos.LoadFromFile(ARCHIVO_CODIGOS, sfJSON);
end;

end.
