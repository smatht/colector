unit PubSub.Subscriber;

interface

uses PubSub.Interfaces;

type
  TPubSubSubscriber = class (TInterfacedObject, ISubscriberInterface)
  private
    fUpdateMethod: TUpdateSubscriberMethod;
  public
    procedure UpdateSubscriber (const notifyClass: INotificationClass);
    procedure SetUpdateSubscriberMethod (newMethod: TUpdateSubscriberMethod);
  end;

implementation

{ TPubSubSubscriber }

procedure TPubSubSubscriber.SetUpdateSubscriberMethod(
  newMethod: TUpdateSubscriberMethod);
begin
  fUpdateMethod:=newMethod;
end;

procedure TPubSubSubscriber.UpdateSubscriber(const notifyClass: INotificationClass);
begin
  if Assigned(fUpdateMethod) then
    fUpdateMethod(notifyClass);
end;

end.