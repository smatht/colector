unit PubSub.Notifications;

interface

uses
  PubSub.Interfaces, PubSub.InterfaceActions, Globales;

type
  TNotificationClass = class (TInterfacedObject, INotificationClass)
    private
      fActions: TInterfaceActions;
      fActionValue: String;
    public
      property Actions: TInterfaceActions read fActions write fActions;
      property ActionValue: string read fActionValue write fActionValue;
    end;

  TNotificationCodigoClass = class (TInterfacedObject, INotificationClass)
  private
    fActions: TInterfaceActions;
    fActionValue: TCodigoColectado;
  public
    property Actions: TInterfaceActions read fActions write fActions;
    property ActionValue: TCodigoColectado read fActionValue write fActionValue;
  end;

implementation

end.