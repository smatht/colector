USE [Facor]
GO

/****** Object:  Table [dbo].[TMPPreciosColectora]    Script Date: 26/02/2020 14:19:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[TMPPreciosColectora](
	[id] [varchar](50) NOT NULL,
	[sCodigoBarra] [varchar](50) NOT NULL,
	[fToma] [datetime] NULL,
	[rImporte] [real] NULL
) ON [PRIMARY]
GO


CREATE TABLE TMPComprasColectora
(idTipoDoc VARCHAR(3) NOT NULL,
 idProveedor INT NOT NULL,
 idSucursal INT NOT NULL,
 idNumDoc INT NOT NULL,
 idNumLinea INT NOT NULL,
 idProducto INT NOT NULL,
 rCantidadRecepcionada REAL NOT NULL,
 fProceso DATETIME NOT NULL DEFAULT GETDATE())
 GO

--SELECT * FROM TMPComprasColectora
EXEC getProveedoresColector

SELECT fDocumento, cast(fDocumento AS float) FROM Compras

SELECT * FROM PuntosVenta