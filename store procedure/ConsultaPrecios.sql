-- =============================================
 -- Author:              Esteban Vignolo
 -- Create date: 18-09-2014
 -- Description: Consulta de precios
 -- =============================================
alter PROCEDURE [dbo].[ConsultaPrecios]
  @sCodBarra varchar(50) = NULL
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @RC int
	DECLARE @idLote int
	DECLARE @idLista smallint
	DECLARE @idListaExcept smallint
	DECLARE @idDeposito smallint
	DECLARE @idUnidad smallint
	DECLARE @idProducto int
	DECLARE @fFecha datetime
	DECLARE @iVigencia int
	DECLARE @fDesde DATETIME
    DECLARE @idCliente INT
	DECLARE @idPV INT
	-- TODO: Set parameter values here.
	SET @idLote = NULL
	SET @idLista = 1
	SET @idListaExcept = NULL
	SET @idDeposito = 1
	SET @idUnidad = 1
	SET @idProducto = (SELECT  ISNULL(min(idProducto),-1) from Productos where sCodBarra=@sCodBarra or sCodBarra2=@sCodBarra)
    SET @idCliente  = (SELECT  ISNULL(min(idCliente),-1) from clientes where bactivo=1)
	SET @idPV  = (SELECT  ISNULL(min(idPV),-1) from PuntosVenta where bActivo=1)
	SET @fFecha = GETDATE()
	SET @iVigencia = 1
	SET @fDesde = NULL
 -- Query principal
DECLARE  @ListaBase TABLE (
	idLista smallint, 
	idLote int, 
	idDeposito smallint, 
	idUnidad smallint, 
	Deposito varchar(50), 
	idProducto int,
	sCodProducto varchar(12),
	sCodProveedor varchar(50),  
	sNombre varchar(50), 
	idFamilia smallint,
	idProveedor smallint, 
	iCantidad real, 
	sShortDescrip varchar(3), 
	sCodLote varchar(12), 
	rPorcentaje money, 
	rPrecioVenta money,
	fVigencia datetime, 
	Usuario_Creacion varchar(10), 
	Fecha_Creacion datetime, 
	Usuario_Modificacion varchar(10), 
	Fecha_Modificacion datetime,
	Lista varchar(50), 
	Familia varchar(50), 
	idUnidadPredet smallint,
	iGrupo smallint,
    sCodBarra varchar(13),
    sCodBarra2 varchar(13),
    idLinea INT
 )
	INSERT @ListaBase (idLista, idLote, idDeposito, idUnidad, Deposito, idProducto,sCodProducto,sCodProveedor,
				sNombre, idFamilia, idProveedor, iCantidad, sShortDescrip, sCodLote, rPorcentaje, 
				rPrecioVenta, fVigencia, Usuario_Creacion, Fecha_Creacion, Usuario_Modificacion, 
				Fecha_Modificacion, Lista, Familia, idUnidadPredet, iGrupo, sCodBarra, sCodBarra2, idLinea)
     		EXECUTE @RC = [dbo].[getPrecios]
			@idLote
		   ,@idLista
		   ,@idListaExcept
		   ,@idDeposito
		   ,@idUnidad
		   ,@idProducto
		   ,@fFecha
		   ,@iVigencia
		   ,@fDesde
	SELECT CASE WHEN L.bIncluyeImpuestos=1 THEN LB.rPrecioVenta ELSE  dbo.getPrecioConImpuesto(LB.rPrecioVenta,LB.idLista,LB.idProducto,@idCliente,@idPV,@idUnidad,@fFecha) END AS rPrecioVenta,
	                 LB.sCodProducto, LB.idProducto, LB.sNombre
	FROM @ListaBase LB, Listas L
	WHERE LB.idLista=L.idLista
END
--SELECT * FROM Productos WHERE sCodBarra=‘7790387010305’
--EXEC consultaprecios '7798090070333'
--EXEC consultaprecios '7790387010305'
GO