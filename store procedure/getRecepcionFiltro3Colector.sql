ALTER PROCEDURE [dbo].[getRecepcionFiltro3Colector] (@ID_F1 VARCHAR(20),@ID_F2 VARCHAR(20))
AS
BEGIN 
   SELECT CAST(idProducto AS VARCHAR(20)) AS Id, sNombre AS Descripcion, sCodBarra CodBarra
   FROM Productos 
   WHERE bActivo=1
    AND idProveedor=CAST(@ID_F1 AS INT)
	AND idFamilia=CAST(@ID_F1 AS INT)
END
