ALTER PROCEDURE [dbo].[getProveedoresColector] 
AS
BEGIN --EXEC getProveedoresColector
  declare @idSucCaja INT
  SET @idSucCaja=1;   
  IF OBJECT_ID('MSsubscription_properties') IS NOT NULL
   BEGIN
    SET @idSucCaja=IsNull((SELECT MIN(hostname) FROM MSsubscription_properties),1)
   END

	SELECT PRO.idProveedor, PRO.sRazonSocial
	FROM Proveedores PRO
	WHERE PRO.bActivo=1
	AND EXISTS (SELECT 1 FROM Compras C 
				 INNER JOIN Proveedores P on (C.idProveedor = P.idProveedor)
				 INNER JOIN TiposDoc TD ON (C.idTipoDoc = TD.idTipoDoc) 
				WHERE TD.iTipo = 7 AND ISNULL(C.idProveedor,@idSucCaja)=@idSucCaja
				 AND C.bFinalizado = 0 AND C.idProveedor=PRO.idProveedor 
				 AND NOT EXISTS (SELECT 1 FROM TMPComprasItems TMP
								 WHERE TMP.IdRefProveedor=C.idProveedor
								  AND TMP.IdRefTipoDoc=C.idTipoDoc
								  AND TMP.IdRefNumDoc=C.idNumDoc
								  AND TMP.IdRefSucursal=C.idsucursal)
    				 AND NOT EXISTS (SELECT 1 FROM TMPComprasColectora TMP
								 WHERE TMP.idProveedor=C.idProveedor
								  AND TMP.idTipoDoc=C.idTipoDoc
								  AND TMP.idNumDoc=C.idNumDoc
								  AND TMP.idsucursal=C.idsucursal)) 
	ORDER BY PRO.sRazonSocial ASC
END
 
