ALTER PROCEDURE [dbo].[getRecepcionFiltro2Colector] (@ID_F1 VARCHAR(20))
AS
BEGIN 
   SELECT CAST(idFamilia AS VARCHAR(20)) AS Id, sNombre AS Descripcion
   FROM Familias F
   WHERE EXISTS (SELECT 1 FROM Productos P 
                 WHERE P.idFamilia=F.idFamilia AND P.idProveedor=CAST(@ID_F1 AS INT))
END
