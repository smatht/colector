ALTER PROCEDURE [dbo].[getRecepcionMercaderiaCabecera]
AS
BEGIN
  
  declare @idSucCaja INT
  SET @idSucCaja=1;   
  IF OBJECT_ID('MSsubscription_properties') IS NOT NULL
   BEGIN
    SET @idSucCaja=IsNull((SELECT MIN(hostname) FROM MSsubscription_properties),1)
   END

	SELECT (C.idTipoDoc+'-'+RIGHT('0000' + Cast(C.idSucursal as Varchar),4)+'-'+RIGHT('00000000' + Cast(C.idNumDoc as Varchar),8) ) AS sNumDoc,
	C.fDocumento, C.idTipoDoc, C.idProveedor, C.idsucursal, C.idNumDoc, P.sRazonSocial
	FROM Compras C 
	 INNER JOIN Proveedores P on (C.idProveedor = P.idProveedor)
	 INNER JOIN TiposDoc TD ON (C.idTipoDoc = TD.idTipoDoc) 
	WHERE TD.iTipo = 7 AND ISNULL(C.idSucuCaja,@idSucCaja)=@idSucCaja
	 AND C.bFinalizado = 0 
	 AND NOT EXISTS (SELECT 1 FROM TMPComprasItems TMP
					 WHERE TMP.IdRefProveedor=C.idProveedor
					  AND TMP.IdRefTipoDoc=C.idTipoDoc
					  AND TMP.IdRefNumDoc=C.idNumDoc
					  AND TMP.IdRefSucursal=C.idsucursal)
    	 AND NOT EXISTS (SELECT 1 FROM TMPComprasColectora TMP
					 WHERE TMP.idProveedor=C.idProveedor
					  AND TMP.idTipoDoc=C.idTipoDoc
					  AND TMP.idNumDoc=C.idNumDoc
					  AND TMP.idsucursal=C.idsucursal)
   ORDER BY C.fDocumento DESC
  END
GO 



